package net.ihe.gazelle.ssov7.authn.interlay;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * This class will provide all the enabled Authenticators using SPI.
 *
 * @see AuthenticatorFactory
 * @see Authenticator
 */
@Scope(ScopeType.APPLICATION)
@AutoCreate
@Name("authenticatorProvider")
public class AuthenticatorProvider{

    List<AuthenticatorFactory> factories = new ArrayList<>();

    public List<Authenticator> getEnabledAuthenticators() {
        List<Authenticator> authenticators = new ArrayList<>();
        for (AuthenticatorFactory authenticatorFactory : getFactories()) {
            if (authenticatorFactory.isEnabled()) {
                authenticators.add(authenticatorFactory.createAuthenticator());
            }
        }
        return authenticators;
    }

    private Iterable<AuthenticatorFactory> getFactories() {
        if (factories.isEmpty()) {
            for (AuthenticatorFactory authenticatorFactory : ServiceLoader.load(AuthenticatorFactory.class)) {
                factories.add(authenticatorFactory);
            }
        }
        return factories;
    }
}
