package net.ihe.gazelle.ssov7.authn.interlay.client;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerErrorRetryStrategy implements ServiceUnavailableRetryStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(ServerErrorRetryStrategy.class);

    private final int timeout;
    private final int maxRetries;

    public ServerErrorRetryStrategy(int timeout, int maxRetries) {
        this.timeout = timeout;
        this.maxRetries = maxRetries;
    }

    @Override
    public boolean retryRequest(HttpResponse response, int executionCount, HttpContext context) {
        LOG.trace("Retry request because of error {} {}",
                response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());

        return response.getStatusLine().getStatusCode() >= HttpStatus.SC_INTERNAL_SERVER_ERROR
                && executionCount <= maxRetries;
    }

    @Override
    public long getRetryInterval() {
        return timeout;
    }
}
