package net.ihe.gazelle.ssov7.gum.client.interlay.ws;

import net.ihe.gazelle.ssov7.gum.client.application.User;

import java.sql.Timestamp;
import java.util.*;

public class UserResource {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private Set<String> groupIds = new HashSet<>();
    private String organizationId;
    private OrganizationResource organization;
    private String password;
    private String passwordConfirmation;
    private Boolean activated;
    private Timestamp lastLoginTimestamp;
    private Integer loginCounter;
    private Timestamp lastUpdateTimestamp;
    private boolean consent;
    private String externalId;
    private String idpId;

    public UserResource() {
        // Empty constructor needed for Jackson.
    }

    public UserResource(String id) {
        this.id = id;
    }

    public UserResource(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        if (user.getGroupIds() != null && !user.getGroupIds().isEmpty()) {
            this.groupIds = new HashSet<>(user.getGroupIds());
        }
        this.organizationId = user.getOrganizationId();
        this.activated = user.getActivated();
        this.lastLoginTimestamp = user.getLastLoginTimestamp();
        this.loginCounter = user.getLoginCounter();
        this.lastUpdateTimestamp = user.getLastUpdateTimestamp();
        this.externalId = user.getExternalId();
        this.idpId = user.getIdpId();
    }

    public UserResource(String firstName, String lastName, String organizationId, String group, boolean activated) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.organizationId = organizationId;
        this.groupIds = new HashSet<>();
        if (group != null) {
            groupIds.add(group);
        }
        this.activated = activated;
    }

    public UserResource(User user, OrganizationResource group) {
        this(user);
        if (group != null)
            this.organization = group;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getGroupIds() {
        return new HashSet<>(groupIds);
    }

    public void setGroupIds(Set<String> groups) {
        this.groupIds = groups == null
                ? new HashSet<String>()
                : new HashSet<>(groups);
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Boolean isActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Timestamp getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public OrganizationResource getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationResource organization) {
        this.organization = organization;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public Integer getLoginCounter() {
        return loginCounter;
    }

    public void setLoginCounter(Integer loginCounter) {
        this.loginCounter = loginCounter;
    }

    public Timestamp getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public boolean getConsent() { return consent; }

    public void setConsent(boolean consent) { this.consent = consent; }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getIdpId() {
        return idpId;
    }

    public void setIdpId(String idpId) {
        this.idpId = idpId;
    }

    public User asUser() {
        User user = new User();
        user.setFirstName(this.firstName);
        user.setLastName(this.lastName);
        user.setEmail(this.email);
        if (this.groupIds != null)
            user.setGroupIds(this.groupIds);
        if (this.organizationId != null)
            user.setOrganizationId(this.organizationId);
        user.setActivated(this.activated);
        user.setLastLoginTimestamp(this.lastLoginTimestamp);
        user.setLoginCounter(this.loginCounter);
        user.setLastUpdateTimestamp(this.lastUpdateTimestamp);
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResource that = (UserResource) o;
        return Objects.equals(consent, that.consent)
                && Objects.equals(id, that.id)
                && Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName)
                && Objects.equals(email, that.email)
                && Objects.equals(groupIds, that.groupIds)
                && Objects.equals(organizationId, that.organizationId)
                && Objects.equals(organization, that.organization)
                && Objects.equals(password, that.password)
                && Objects.equals(passwordConfirmation, that.passwordConfirmation)
                && Objects.equals(activated, that.activated)
                && Objects.equals(lastLoginTimestamp, that.lastLoginTimestamp)
                && Objects.equals(loginCounter, that.loginCounter)
                && Objects.equals(lastUpdateTimestamp, that.lastUpdateTimestamp)
                && Objects.equals(externalId, that.externalId)
                && Objects.equals(idpId, that.idpId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, groupIds, organizationId, organization, password, passwordConfirmation, activated, lastLoginTimestamp, loginCounter, lastUpdateTimestamp, consent, externalId, idpId);
    }

    @Override
    public String toString() {
        String userResource = this.organization != null ? this.organization.getId() : null;
        return "{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", groupIds=" + groupIds.toString() +
                ", organizationId='" + organizationId + '\'' +
                ", organization='" + userResource + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", activated=" + activated +
                ", lastLoginTimestamp=" + lastLoginTimestamp +
                ", loginCounter=" + loginCounter +
                ", lastUpdateTimestamp=" + lastUpdateTimestamp +
                ", consent=" + consent +
                ", externalId=" + externalId +
                ", idpId=" + idpId +
                '}';
    }
}

