package net.ihe.gazelle.ssov7.gum.client.application.service;

import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;

import java.util.NoSuchElementException;

public interface UserPreferencesService {

    /**
     * Gets user preferences by user id.
     *
     * @param userId the user id
     * @return the user preferences by user id
     * @throws NoSuchElementException         if the user is not found
     * @throws GumSsoClientHttpErrorException if it fails to retrieve preferences
     */
    UserPreference getUserPreferencesByUserId(String userId);

    /**
     * Gets single user preference.
     *
     * @param userID         the user id
     * @param preferenceName the preference name
     * @return the single user preference
     * @throws NoSuchElementException         if the user is not found
     * @throws GumSsoClientHttpErrorException if it fails to retrieve preference
     */
    Object getSingleUserPreference(String userID, String preferenceName);


    /**
     * Get user profile picture as bytes.
     *
     * @param userId the id of the user ot get the picture
     * @param format the format of the image i.e. normal or thumbnail
     * @return the picture bytes
     */
    byte[] getUserProfilePicture(String userId, String format);

    /**
     * Update user preferences user preference.
     *
     * @param userId         the user id
     * @param userPreference the user preference
     * @return the user preference
     * @throws NoSuchElementException         if the user is not found
     * @throws GumSsoClientHttpErrorException if it fails to update the preference
     */
    UserPreference updateUserPreferences(String userId, UserPreference userPreference);

}
