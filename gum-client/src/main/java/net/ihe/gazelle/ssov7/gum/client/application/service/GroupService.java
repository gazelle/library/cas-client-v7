package net.ihe.gazelle.ssov7.gum.client.application.service;

import net.ihe.gazelle.ssov7.gum.client.application.Group;

import java.util.List;

public interface GroupService {

    /**
     * Retrieve all the available Gazelle groups
     * @return a list of groups
     */
    List<Group> getGroups();

}
