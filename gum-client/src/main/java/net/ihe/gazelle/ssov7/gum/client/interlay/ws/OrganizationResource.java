package net.ihe.gazelle.ssov7.gum.client.interlay.ws;

import java.util.Objects;

public class OrganizationResource {

    private String id;
    private String name;
    private String url;
    private String externalId;
    private String idpId;

    public OrganizationResource() {
    }

    public OrganizationResource(String id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public OrganizationResource(String id, String name, String url, String externalId, String idpId) {
        this(id, name, url);
        this.externalId = externalId;
        this.idpId = idpId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDelegated() {
        return externalId != null && idpId != null;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getIdpId() {
        return idpId;
    }

    public void setIdpId(String idpId) {
        this.idpId = idpId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationResource that = (OrganizationResource) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(url, that.url)
                && Objects.equals(externalId, that.externalId)
                && Objects.equals(idpId, that.idpId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, url, externalId, idpId);
    }

    @Override
    public String toString() {
        return "OrganizationResource{" + "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", externalId='" + externalId + '\'' +
                ", idpId='" + idpId + '\'' +
                '}';
    }
}
