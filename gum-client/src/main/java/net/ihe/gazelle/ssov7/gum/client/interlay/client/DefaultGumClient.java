package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.SsoServerErrorRetryHandler;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.SsoServerErrorRetryStrategy;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import net.ihe.gazelle.ssov7.m2m.client.interlay.client.M2MHttpClientBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public class DefaultGumClient {

    private static final int RETRY_COUNT = 3;
    private static final String GUM_REST_API_URL = "GUM_REST_API_URL";
    private final String gumUrl;
    private final int retryInterval;

    public DefaultGumClient() {
        gumUrl = assertNonNull(System.getenv(GUM_REST_API_URL)) + "/rest";
        //retry interval between request when server error, defaulting with 30 seconds
        String ssoClientRetryInterval = System.getenv("SSO_CLIENT_RETRY_INTERVAL");
        retryInterval = ssoClientRetryInterval == null ? 30000 : Integer.parseInt(ssoClientRetryInterval);
    }

    public CloseableHttpClient buildCloseableHttpClient() {
        return M2MHttpClientBuilder.createWithAccessToken()
                .setServiceUnavailableRetryStrategy(
                        new SsoServerErrorRetryStrategy(getRetryInterval(), RETRY_COUNT))
                .setRetryHandler(new SsoServerErrorRetryHandler(RETRY_COUNT))
                .build();
    }

    public CloseableHttpClient buildCloseableHttpClient(List<String> scopes) {
        return M2MHttpClientBuilder.createWithAccessToken(scopes)
                .setServiceUnavailableRetryStrategy(
                        new SsoServerErrorRetryStrategy(getRetryInterval(), RETRY_COUNT))
                .setRetryHandler(new SsoServerErrorRetryHandler(RETRY_COUNT))
                .build();
    }

    public void assertNoErrorStatus(int statusCode, String statusText, HttpEntity responseEntity) throws IOException {
        if (statusCode >= HttpStatus.SC_BAD_REQUEST) {
            ErrorResponse errorResponse;
            try {
                errorResponse = extractJsonValue(responseEntity, new TypeReference<ErrorResponse>() {});
            } catch (MismatchedInputException e) {
                errorResponse = new ErrorResponse();
            }
            String message = (statusCode + " " + statusText + " " + errorResponse.toString()).trim();
            if (statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new NoSuchElementException(message);
            }
            throw new GumSsoClientHttpErrorException(message);
        }
    }

    private String assertNonNull(String value) {
        if (value != null && !value.isEmpty() && !" ".equals(value)) {
            return value;
        } else {
            throw new IllegalStateException("Missing environment variable " + GUM_REST_API_URL);
        }
    }

    public <T> T extractJsonValue(HttpEntity responseEntity, TypeReference<T> typeReference) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        // Do not wrap getContent inputStream in try-with-resources block, or it will mess stream content.
        // Stream is closed by calling EntityUtils.consumeQuietly.
        return objectMapper.readValue(responseEntity.getContent(), typeReference);
    }

    public <T> List<T> extractJsonArrayResponse(HttpEntity responseEntity, Class<T> gumClass) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<T> gumObjectList = new ArrayList<>();
        try (InputStream inputStream = responseEntity.getContent()) {
            JsonNode jsonArray = objectMapper.readTree(inputStream);
            if (!jsonArray.isEmpty()) {
                for (JsonNode element : jsonArray) {
                    T gumObject = objectMapper.treeToValue(element, gumClass);
                    gumObjectList.add(gumObject);
                }
            } else {
                gumObjectList = Collections.emptyList();
            }
        }
        EntityUtils.consume(responseEntity);
        return gumObjectList;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public int getRetryCount() {
        return RETRY_COUNT;
    }

    public String getGumUrl() {
        return gumUrl;
    }

    private static class ErrorResponse {
        private String error = "";
        private String code = "";
        private String message = "";

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return (error + " " + message).trim();
        }
    }
}
