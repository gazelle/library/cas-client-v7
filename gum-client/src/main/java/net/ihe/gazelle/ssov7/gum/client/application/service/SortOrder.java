package net.ihe.gazelle.ssov7.gum.client.application.service;

public enum SortOrder {
    ASC,
    DESC;

    SortOrder() {
    }

    public static SortOrder getByStringOrDefault(String sortOrder) {
        if (DESC.name().equals(sortOrder)) {
            return DESC;
        }
        return ASC;
    }
}