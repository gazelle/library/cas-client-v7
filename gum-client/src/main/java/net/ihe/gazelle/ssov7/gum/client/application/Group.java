package net.ihe.gazelle.ssov7.gum.client.application;

import java.util.HashSet;
import java.util.Set;

public class Group {

    public static final String GRP_ORGA_ADMIN = "org-adm";
    public static final String GRP_ORGA_MEMBER = "org";
    public static final String GRP_MONITOR = "role:monitor";
    public static final String GRP_GAZELLE_ADMIN = "role:gazelle_admin";
    public static final String GRP_TEST_DESIGNER = "role:test_designer";
    public static final String GRP_TESTING_SESSION_MANAGER = "role:testing_session_manager";
    public static final String GRP_LATE_REGISTRATION = "role:late_registration";
    public static final String GRP_PROJECT_ADMIN = "role:project_admin";

    private String reference;
    private String type;
    private Set<String> groupIds;

    public Group() {
        this.groupIds = new HashSet<>();
    }

    public Group(String type, String reference) {
        this();
        this.type = type;
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return type + ":" + reference;
    }

    public Set<String> getGroupIds() {
        return new HashSet<>(groupIds);
    }

    public void setGroupIds(Set<String> groupIds) {
        this.groupIds = groupIds == null
                ? new HashSet<String>()
                : new HashSet<>(groupIds);
    }
}
