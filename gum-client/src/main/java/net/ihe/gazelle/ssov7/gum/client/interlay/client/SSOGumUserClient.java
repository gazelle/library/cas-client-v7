package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import net.ihe.gazelle.ssov7.gum.client.interlay.model.entity.UserSearchResult;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author Clément LAGORCE, Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 29/08/2023
 */
public class SSOGumUserClient implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SSOGumUserClient.class);
    public static final int NO_LIMIT_INCREMENT_DEFAULT = 500;
    private static final String UNKNOWN_USER = "Unknown user";
    private final DefaultGumClient gumClient;
    private final String userEndPoint ;
    private final int noLimitIncrement;

    public SSOGumUserClient() {
        this(NO_LIMIT_INCREMENT_DEFAULT);
    }

    public SSOGumUserClient(int noLimitIncrement) {
        super();
        this.noLimitIncrement = noLimitIncrement;
        gumClient = new DefaultGumClient();
        userEndPoint= gumClient.getGumUrl() + "/users";
    }


    @Override
    public UserSearchResult searchAndFilter(UserSearchParams userSearchParams) {
        LOGGER.trace("searchAndFilter");
        try (CloseableHttpClient client = gumClient.buildCloseableHttpClient()) {
            HttpGet get = getFilterHttpGetRequest(userEndPoint, userSearchParams);
            HttpEntity responseEntity = null;
            try (CloseableHttpResponse response = client.execute(get)) {
                responseEntity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseEntity);
                return gumClient.extractJsonValue(responseEntity, new TypeReference<UserSearchResult>() {});
            } finally {
                EntityUtils.consumeQuietly(responseEntity);
            }
        } catch (IOException e) {
            throw new GumSsoClientHttpErrorException("searchAndFilter failed", e);
        }
    }

    @Override
    public List<User> searchNoLimit(UserSearchParams userSearchParams) {
        List<User> users = new ArrayList<>();
        UserSearchResult searchResult;
        int offset = 0;
        int limit = noLimitIncrement;
        userSearchParams.setLimit(limit);
        do {
            userSearchParams.setOffset(offset);
            searchResult = searchAndFilter(userSearchParams);
            users.addAll(searchResult.getUsers());
            offset += limit;
        } while (users.size() < searchResult.getCount());
        return users;
    }

    @Override
    public Map<String, Long> getValueCount(String propertyName, String search, String organizationId, String role, Boolean activated) {
        LOGGER.trace("getValueCount");
        try (CloseableHttpClient client = gumClient.buildCloseableHttpClient()) {
            HttpGet get = getFilterHttpGetRequest(userEndPoint + "/" + propertyName + "/count", search, null, null,
                    null, organizationId, role, activated, null, null, null, null, null, null, null);
            HttpEntity responseEntity = null;
            try (CloseableHttpResponse response = client.execute(get)) {
                responseEntity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseEntity);
                return gumClient.extractJsonValue(responseEntity, new TypeReference<Map<String, Long>>() {
                });
            } finally {
                EntityUtils.consumeQuietly(responseEntity);
            }
        } catch (IOException e) {
            throw new GumSsoClientHttpErrorException("getValueCount failed", e);
        }
    }

    @Override
    public User getUserById(String userId) {
        LOGGER.trace("getUserById");
        if (userId != null && !userId.isEmpty()) {
            try (CloseableHttpClient httpClient = gumClient.buildCloseableHttpClient()) {

                HttpGet clientsRequest = new HttpGet(userEndPoint + "/" + URLEncoder.encode(userId, StandardCharsets.UTF_8.name()));

                HttpEntity responseEntity = null;
                try (CloseableHttpResponse response = httpClient.execute(clientsRequest)) {
                    responseEntity = response.getEntity();
                    gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseEntity);
                    InputStream inputStream = responseEntity.getContent();
                    String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
                    return new ObjectMapper().readValue(result, User.class);
                } finally {
                    if (responseEntity != null)
                        EntityUtils.consumeQuietly(responseEntity);
                }
            } catch (IOException e) {
                throw new GumSsoClientHttpErrorException(String.format("getUserById failed with userId %s", userId), e);
            }
        } else {
            throw new IllegalArgumentException("Fail to get user cause userId is null or empty");
        }
    }



    @Override
    public void removeUserCache(String userId) {
        // There is no cache to remove.
    }


    @Override
    public String getUserDisplayNameWithoutException(String userId) {
        if(userId != null && !userId.isEmpty()) {
            try {
                return getUserById(userId).getFirstNameAndLastName();
            } catch (NoSuchElementException e) {
                LOGGER.info("Unable to retrieve user {} : {} ", userId, e.getMessage());
                return UNKNOWN_USER;
            } catch (Exception e) {
                LOGGER.warn("Unable to retrieve user {} : ", userId, e);
                return UNKNOWN_USER;
            }
        } else {
            return UNKNOWN_USER;
        }
    }

    private HttpGet getFilterHttpGetRequest(String endPoint, UserSearchParams userSearchParams) {
        Map<String, String> requestMap = new LinkedHashMap<>();

        try {
            prepareParam("search", userSearchParams.getSearch(), requestMap);
            prepareParam("firstName", userSearchParams.getFirstName(), requestMap);
            prepareParam("lastName", userSearchParams.getLastName(), requestMap);
            prepareParam("email", userSearchParams.getEmail(), requestMap);
            prepareParam("organizationId", userSearchParams.getOrganizationId(), requestMap);
            prepareParam("group", userSearchParams.getGroup(), requestMap);
            prepareParam("activated", userSearchParams.getActivated(), requestMap);
            prepareParam("delegated", userSearchParams.getDelegated(), requestMap);
            prepareParam("externalId", userSearchParams.getExternalId(), requestMap);
            prepareParam("idpId", userSearchParams.getIdpId(), requestMap);
            prepareParam("offset", userSearchParams.getOffset(), requestMap);
            prepareParam("limit", userSearchParams.getLimit(), requestMap);
            prepareParam("sortBy", userSearchParams.getSortBy(), requestMap);
            prepareParam("sortOrder", userSearchParams.getSortOrder(), requestMap);

            URI requestURI = encodeURI(endPoint, requestMap);
            return new HttpGet(requestURI);
        } catch (URISyntaxException e) {
            throw new GumSsoClientHttpErrorException("Bad encoding one of the parameters.");
        }
    }

    private HttpGet getFilterHttpGetRequest(String endPoint, String search,
                                            String firstName, String lastName, String email,
                                            String organizationId, String role, Boolean activated,
                                            Boolean delegated, String externalId, String idpId,
                                            Integer offset, Integer limit, String sortBy, SortOrder sortOrder) {
        Map<String, String> requestMap = new LinkedHashMap<>();

        try {
            prepareParam("search", search, requestMap);
            prepareParam("firstName", firstName, requestMap);
            prepareParam("lastName", lastName, requestMap);
            prepareParam("email", email, requestMap);
            prepareParam("organizationId", organizationId, requestMap);
            prepareParam("role", role, requestMap);
            prepareParam("activated", activated, requestMap);
            prepareParam("delegated", delegated, requestMap);
            prepareParam("externalId", externalId, requestMap);
            prepareParam("idpId", idpId, requestMap);
            prepareParam("offset", offset, requestMap);
            prepareParam("limit", limit, requestMap);
            prepareParam("sortBy", sortBy, requestMap);
            prepareParam("sortOrder", sortOrder, requestMap);

            URI requestURI = encodeURI(endPoint, requestMap);
            return new HttpGet(requestURI);
        } catch (URISyntaxException e) {
            throw new GumSsoClientHttpErrorException("Bad encoding one of the parameters.");
        }
    }

    private static URI encodeURI(String endPoint, Map<String, String> requestMap) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(endPoint, StandardCharsets.UTF_8);
        for(Map.Entry<String, String> param : requestMap.entrySet()) {
            uriBuilder.addParameter(param.getKey(), param.getValue());
        }
        return uriBuilder.build();
    }

    private static void prepareParam(String paramKey, Object paramValue, Map<String, String> requestMap) {
        if (paramValue != null) {
            requestMap.put(paramKey, String.valueOf(paramValue));
        }
    }

}