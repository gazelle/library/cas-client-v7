package net.ihe.gazelle.ssov7.gum.client.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserPreference {

    private String userId;
    private String profilePictureUri;
    private String profileThumbnailUri;
    private String tableLabel;
    private boolean notifiedByEmail;
    private List<String> languagesSpoken;


    public UserPreference() {
        languagesSpoken = new ArrayList<>();
    }

    public UserPreference(UserPreference userPreference) {
        this.userId = userPreference.getUserId();
        this.profilePictureUri = userPreference.getProfilePictureUri();
        this.profileThumbnailUri = userPreference.getProfileThumbnailUri();
        this.tableLabel = userPreference.getTableLabel();
        this.notifiedByEmail = userPreference.isNotifiedByEmail();
        this.languagesSpoken = userPreference.getLanguagesSpoken();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfilePictureUri() {
        return profilePictureUri;
    }

    public void setProfilePictureUri(String profilePictureUri) {
        this.profilePictureUri = profilePictureUri;
    }

    public String getProfileThumbnailUri() {
        return profileThumbnailUri;
    }

    public void setProfileThumbnailUri(String profileThumbnailUri) {
        this.profileThumbnailUri = profileThumbnailUri;
    }

    public String getTableLabel() {
        return tableLabel;
    }

    public void setTableLabel(String tableLabel) {
        this.tableLabel = tableLabel;
    }

    public boolean isNotifiedByEmail() {
        return notifiedByEmail;
    }

    public void setNotifiedByEmail(boolean notifiedByEmail) {
        this.notifiedByEmail = notifiedByEmail;
    }

    public List<String> getLanguagesSpoken() {
        return new ArrayList<>(languagesSpoken);
    }

    public void setLanguagesSpoken(List<String> languagesSpoken) {
        this.languagesSpoken = languagesSpoken == null
                ? new ArrayList<String>()
                : new ArrayList<>(languagesSpoken);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPreference that = (UserPreference) o;
        return notifiedByEmail == that.notifiedByEmail
                && Objects.equals(userId, that.userId)
                && Objects.equals(profilePictureUri, that.profilePictureUri)
                && Objects.equals(profileThumbnailUri, that.profileThumbnailUri)
                && Objects.equals(tableLabel, that.tableLabel)
                && Objects.equals(languagesSpoken, that.languagesSpoken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId,
                profilePictureUri,
                profileThumbnailUri,
                tableLabel,
                notifiedByEmail,
                languagesSpoken);
    }

    @Override
    public String toString() {
        return "UserPreference{" +
                "userId='" + userId + '\'' +
                ", profilePictureBase64='" + profilePictureUri + '\'' +
                ", profileThumbnailBase64='" + profileThumbnailUri + '\'' +
                ", tableLabel='" + tableLabel + '\'' +
                ", notifiedByEmail=" + notifiedByEmail +
                ", languagesSpoken=" + languagesSpoken +
                '}';
    }
}
