package net.ihe.gazelle.ssov7.gum.client.interlay.factory;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import net.ihe.gazelle.ssov7.gum.client.interlay.cache.UserPreferencesClientCache;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumUserPreferencesClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("userPreferenceServiceFactory")
@Scope(ScopeType.APPLICATION)
@Install
@AutoCreate
public class UserPreferenceServiceFactory {

    private static UserPreferencesClientCache userPreferencesClientCache;

    @Factory(value = "userPreferencesService", autoCreate = true, scope = ScopeType.APPLICATION)
    public static UserPreferencesService getUserPreferenceService() {
        if (userPreferencesClientCache == null) {
            userPreferencesClientCache = new UserPreferencesClientCache(new SSOGumUserPreferencesClient());
        }
        return userPreferencesClientCache;
    }
}
