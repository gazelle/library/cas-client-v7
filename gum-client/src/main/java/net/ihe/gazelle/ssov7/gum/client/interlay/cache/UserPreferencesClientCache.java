package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import org.apache.commons.collections.keyvalue.MultiKey;

import java.util.concurrent.TimeUnit;

public class UserPreferencesClientCache implements UserPreferencesService {

    public static final int EXPIRATION;
    public static final int DEFAULT_EXPIRATION_MINUTES = 10;
    private static final String CANNOT_BE_NULL = " cannot be null";
    private static final String USER_ID = "userId";

    private static final String CACHE_EXPIRATION_MINUTES_ENV = "GZL_USER_CACHE_EXPIRATION_MINUTES";

    static {
        String exp = System.getenv(CACHE_EXPIRATION_MINUTES_ENV);
        EXPIRATION = exp != null ? Integer.parseInt(exp) : DEFAULT_EXPIRATION_MINUTES;
    }

    private final UserPreferencesService userPreferencesService;


    private final LoadingCache<String, UserPreference> userPreferencesCache = CacheBuilder.newBuilder().maximumSize(2000)
            .expireAfterWrite(EXPIRATION, TimeUnit.MINUTES).expireAfterAccess(EXPIRATION, TimeUnit.MINUTES).build(
                    new CacheLoader<String, UserPreference>() {
                        @Override
                        public UserPreference load(String userId) throws Exception {
                            return userPreferencesService.getUserPreferencesByUserId(userId);
                        }
                    }
            );

    private final LoadingCache<MultiKey, byte[]> userPictureCache = CacheBuilder.newBuilder().maximumSize(2000)
            .expireAfterWrite(EXPIRATION, TimeUnit.MINUTES).expireAfterAccess(EXPIRATION, TimeUnit.MINUTES).build(
                    new CacheLoader<MultiKey, byte[]>() {
                        @Override
                        public byte[] load(MultiKey multiKey) throws Exception {
                            String userId = String.valueOf(multiKey.getKey(0));
                            String imageFormat = String.valueOf(multiKey.getKey(1));
                            return userPreferencesService.getUserProfilePicture(userId, imageFormat);
                        }
                    }
            );

    private final LoadingCache<MultiKey, Object> singleUserPreferencesCache = CacheBuilder.newBuilder().maximumSize(2000)
            .expireAfterWrite(EXPIRATION, TimeUnit.MINUTES).expireAfterAccess(EXPIRATION, TimeUnit.MINUTES).build(
                    new CacheLoader<MultiKey, Object>() {
                        @Override
                        public Object load(MultiKey multiKey) throws Exception {
                            String userId = String.valueOf(multiKey.getKey(0));
                            String preferenceName = String.valueOf(multiKey.getKey(1));
                            return userPreferencesService.getSingleUserPreference(userId, preferenceName);
                        }
                    }
            );


    public UserPreferencesClientCache(UserPreferencesService userPreferencesService) {
        this.userPreferencesService = userPreferencesService;
    }

    @Override
    public UserPreference getUserPreferencesByUserId(String userId) {
        if (userId == null)
            throw new IllegalArgumentException("userId cannot be null");
        return userPreferencesCache.getUnchecked(userId);
    }

    @Override
    public Object getSingleUserPreference(String userId, String preferenceName) {
        if (userId == null || preferenceName == null)
            throw new IllegalArgumentException((userId == null ? USER_ID : "preferenceName") + CANNOT_BE_NULL);
        return singleUserPreferencesCache.getUnchecked(new MultiKey(userId, preferenceName));
    }

    @Override
    public byte[] getUserProfilePicture(String userId, String format) {
        if (userId == null || format == null)
            throw new IllegalArgumentException((userId == null ? USER_ID : "format") + CANNOT_BE_NULL);
        return userPictureCache.getUnchecked(new MultiKey(userId, format));
    }

    @Override
    public UserPreference updateUserPreferences(String userId, UserPreference userPreference) {
        if (userId == null || userPreference == null)
            throw new IllegalArgumentException((userId == null ? USER_ID : "userPreference") + CANNOT_BE_NULL);
        UserPreference updatedUserPreference = userPreferencesService.updateUserPreferences(userId, userPreference);
        if (updatedUserPreference != null)
            userPreferencesCache.put(userId, updatedUserPreference);
        return updatedUserPreference;
    }
}
