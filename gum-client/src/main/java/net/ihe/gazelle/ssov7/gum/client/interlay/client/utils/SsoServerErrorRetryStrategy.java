package net.ihe.gazelle.ssov7.gum.client.interlay.client.utils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SsoServerErrorRetryStrategy implements ServiceUnavailableRetryStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(SsoServerErrorRetryStrategy.class);

    private final int timeout;
    private final int maxRetries;

    public SsoServerErrorRetryStrategy(int timeout, int maxRetries) {
        this.timeout = timeout;
        this.maxRetries = maxRetries;
    }

    @Override
    public boolean retryRequest(HttpResponse response, int executionCount, HttpContext context) {
        LOG.trace("Retry request because of error {} {}",
                response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());

        return response.getStatusLine().getStatusCode() >= HttpStatus.SC_INTERNAL_SERVER_ERROR
                && executionCount <= maxRetries;
    }

    @Override
    public long getRetryInterval() {
        return timeout;
    }
}
