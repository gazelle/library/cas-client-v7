package net.ihe.gazelle.ssov7.gum.client.application;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Clément LAGORCE
 * @company KEREVAL
 * @project sso-client-v7
 * @date 25/10/2023
 * @deprecated 12/08/24 Gazelle users has only groups now
 */
@Deprecated
public class Role {

    public static final String ADMIN = "admin_role";
    public static final String MONITOR = "monitor_role";
    public static final String PROJECT_MANAGER = "project-manager_role";
    public static final String ACCOUNTING = "accounting_role";
    public static final String VENDOR_ADMIN = "vendor_admin_role";
    public static final String VENDOR = "vendor_role";
    public static final String USER = "user_role";
    public static final String TF_EDITOR = "tf_editor_role";
    public static final String TESTS_EDITOR = "tests_editor_role";
    public static final String INSTITUTIONS_EDITOR = "institutions_editor_role";
    public static final String VENDOR_LATE_REGISTRATION = "vendor_late_registration_role";
    public static final String TESTING_SESSION_ADMIN = "testing_session_admin_role";

    private String name;
    private String description;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<String> values() {
        List<String> allRoles = new ArrayList<>();
        allRoles.add(ADMIN);
        allRoles.add(MONITOR);
        allRoles.add(PROJECT_MANAGER);
        allRoles.add(ACCOUNTING);
        allRoles.add(VENDOR_ADMIN);
        allRoles.add(VENDOR);
        allRoles.add(USER);
        allRoles.add(TF_EDITOR);
        allRoles.add(TESTS_EDITOR);
        allRoles.add(INSTITUTIONS_EDITOR);
        allRoles.add(VENDOR_LATE_REGISTRATION);
        allRoles.add(TESTING_SESSION_ADMIN);
        return allRoles;
    }

    public static boolean isUserVendorAdmin(User user) {
        return userHasRole(user, Role.VENDOR_ADMIN);
    }

    private static boolean userHasRole(User user, String roleString) {
        return user != null && user.getGroupIds() != null && user.hasRole(roleString);
    }
}
