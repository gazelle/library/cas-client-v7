package net.ihe.gazelle.ssov7.gum.client.interlay.bean;

public class GumUserProfilePictureBeanException extends RuntimeException {

    public GumUserProfilePictureBeanException() {
        super();
    }

    public GumUserProfilePictureBeanException(String message) {
        super(message);
    }

    public GumUserProfilePictureBeanException(String message, Throwable cause) {
        super(message, cause);
    }

    public GumUserProfilePictureBeanException(Throwable cause) {
        super(cause);
    }
}
