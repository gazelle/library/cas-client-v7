package net.ihe.gazelle.ssov7.gum.client.interlay.exception;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 13/09/2023
 */
public class GumSsoClientHttpErrorException extends RuntimeException{
    private static final long serialVersionUID = 5334940544061791093L;

    public GumSsoClientHttpErrorException() {
    }

    public GumSsoClientHttpErrorException(String message) {
        super(message);
    }

    public GumSsoClientHttpErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public GumSsoClientHttpErrorException(Throwable cause) {
        super(cause);
    }
}
