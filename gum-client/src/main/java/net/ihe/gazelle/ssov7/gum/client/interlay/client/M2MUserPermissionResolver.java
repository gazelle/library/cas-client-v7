package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import net.ihe.gazelle.ssov7.m2m.server.domain.M2MAuthorization;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.intercept.PostActivate;
import org.jboss.seam.security.permission.PermissionResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Name("m2mUserPermissionResolver")
@Scope(ScopeType.APPLICATION)
@AutoCreate
@Install
public class M2MUserPermissionResolver implements PermissionResolver, Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(M2MUserPermissionResolver.class);
    private static final long serialVersionUID = -6031969310955495295L;
    public static final String ANY_USERS = "any-users";
    public static final String MY_ACCOUNT = "my-account";
    public static final String MY_ORGA_USERS = "my-orga-users";
    public static final String CREATE = "create";
    public static final String READ_PRIVATE = "read-private";
    public static final String READ_PUBLIC = "read-public";
    public static final String UPDATE = "update";

    private transient Map<MultiKey, M2MAuthorization> authorizations;


    public M2MUserPermissionResolver() {
        LOG.trace("initAuthorizations");
        authorizations = computeAuthorizations();
    }

    @Create
    public void initAuthorizations() {
        LOG.trace("initAuthorizations");
        authorizations = computeAuthorizations();
    }
    @PostActivate
    public void postActivate() {
        initAuthorizations();
    }

    @Override
    public boolean hasPermission(Object target, String action) {
        MultiKey key = new MultiKey(target, action);
        if (authorizations.get(key) != null) {
            return authorizations.get(key).isGranted();
        }
        return false;
    }

    @Override
    public void filterSetByAction(Set<Object> targets, String action) {
        Set<Object> toRemove = new HashSet<>();
        for (Object target : targets) {
            if (hasPermission(target, action)) {
                toRemove.add(target);
            }
        }
        for (Object target : toRemove) {
            targets.remove(target);
        }
    }

    private Map<MultiKey, M2MAuthorization> computeAuthorizations() {
        Map<MultiKey, M2MAuthorization> result = new HashMap<>();
        result.put(new MultiKey(ANY_USERS, CREATE), M2MUserAuthorizations.ANY_USERS_CREATE);
        result.put(new MultiKey(ANY_USERS, READ_PRIVATE), M2MUserAuthorizations.ANY_USERS_READ_PRIVATE);
        result.put(new MultiKey(ANY_USERS, READ_PUBLIC), M2MUserAuthorizations.ANY_USERS_READ_PUBLIC);
        result.put(new MultiKey(ANY_USERS, UPDATE), M2MUserAuthorizations.ANY_USERS_UPDATE);
        result.put(new MultiKey(MY_ACCOUNT, "read"), M2MUserAuthorizations.MY_ACCOUNT_READ);
        result.put(new MultiKey(MY_ACCOUNT, UPDATE), M2MUserAuthorizations.MY_ACCOUNT_UPDATE);
        result.put(new MultiKey(MY_ORGA_USERS, CREATE), M2MUserAuthorizations.MY_ORGA_USERS_CREATE);
        result.put(new MultiKey(MY_ORGA_USERS, READ_PRIVATE), M2MUserAuthorizations.MY_ORGA_USERS_READ_PRIVATE);
        result.put(new MultiKey(MY_ORGA_USERS, READ_PUBLIC), M2MUserAuthorizations.MY_ORGA_USERS_READ_PUBLIC);
        result.put(new MultiKey(MY_ORGA_USERS, UPDATE), M2MUserAuthorizations.MY_ORGA_USERS_UPDATE);
        return result;

    }
}
