package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.service.GroupService;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * @author Clément LAGORCE
 * @company KEREVAL
 * @project sso-client-v7
 * @date 25/10/2023
 */
@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("ssoGroupClient")
public class SSOGumGroupsClient implements GroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SSOGumGroupsClient.class);
    private final DefaultGumClient gumClient;
    private final String groupEndPoint;

    public SSOGumGroupsClient() {
        gumClient = new DefaultGumClient();
        groupEndPoint = gumClient.getGumUrl() + "/groups";
    }

    @Override
    public List<Group> getGroups() {
        LOGGER.trace("enter getGroups");
        List<Group> groups;
        try (CloseableHttpClient client = gumClient.buildCloseableHttpClient()) {
            HttpGet get = new HttpGet(groupEndPoint);

            HttpEntity responseEntity;
            try (CloseableHttpResponse response = client.execute(get)) {
                responseEntity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseEntity);
                groups = gumClient.extractJsonArrayResponse(responseEntity, Group.class);
                EntityUtils.consume(responseEntity);
            }
        } catch (IOException e) {
            throw new GumSsoClientHttpErrorException("getGroups failed", e);
        }
        return groups;
    }
}
