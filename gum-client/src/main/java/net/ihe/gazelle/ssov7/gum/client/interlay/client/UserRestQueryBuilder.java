package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import net.ihe.gazelle.common.filter.NoHQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.beans.HQLStatistic;
import net.ihe.gazelle.hql.beans.HQLStatisticItem;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.model.entity.UserSearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRestQueryBuilder implements NoHQLQueryBuilder<User> {

    public static final String SEARCH = "search";
    public static final String FIRST_NAME = "firstName";
    public static final String EMAIL = "email";
    public static final String LAST_NAME = "lastName";
    public static final String ORGANIZATION_ID = "organizationId";
    public static final String ROLES = "roles";
    public static final String ACTIVATED = "activated";
    public static final String DELEGATED = "delegated";
    public static final String EXTERNAL_ID = "externalId";
    public static final String IDP_ID = "idpId";
    public static final String SORT_BY = "sortBy";
    public static final String SORT_ORDER = "sortOrder";


    private final UserService userService;
    private final Map<String, String> restrictions = new HashMap<>();
    private Integer offset = null;
    private Integer limit = null;
    private UserSearchResult searchResponse;

    public UserRestQueryBuilder(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void addLike(String propertyName, String filterValue) {
        this.restrictions.put(propertyName, filterValue);
    }

    @Override
    public void addOrder(String propertyName, boolean ascending) {
        this.restrictions.put(SORT_BY, propertyName);
        this.restrictions.put(
                SORT_ORDER,
                ascending ? SortOrder.ASC.name() : SortOrder.DESC.name()
        );
    }

    @Override
    public void addEq(String propertyName, Object dataItem) {
        this.restrictions.put(propertyName, (String) dataItem);
    }

    @Override
    public int getCount() {
        if (searchResponse == null) {
            searchResponse = getNoCacheSearchResult();
        }
        return searchResponse.getCount().intValue();
    }

    @Override
    public List<User> getList() {
        if (searchResponse == null) {
            searchResponse = getNoCacheSearchResult();
        }
        return searchResponse.getUsers();
    }

    private UserSearchResult getNoCacheSearchResult() {
        UserSearchParams userSearchParams = new UserSearchParams()
                .setSearch(getRestriction(SEARCH))
                .setFirstName(getRestriction(FIRST_NAME))
                .setLastName(getRestriction(LAST_NAME))
                .setEmail(getRestriction(EMAIL))
                .setOrganizationId(getRestriction(ORGANIZATION_ID))
                .setGroup(getRestriction(ROLES))
                .setActivated(getBooleanFilter(getRestriction(ACTIVATED)))
                .setDelegated(getBooleanFilter(getRestriction(DELEGATED)))
                .setExternalId(getRestriction(EXTERNAL_ID))
                .setIdpId(getRestriction(IDP_ID))
                .setOffset(offset)
                .setLimit(limit)
                .setSortBy(getRestriction(SORT_BY))
                .setSortOrder(SortOrder.getByStringOrDefault(getRestriction(SORT_ORDER)));

        return userService.searchAndFilter(userSearchParams);
    }

    private String getRestriction(String propertyName) {
        return restrictions.get(propertyName);
    }

    private Boolean getBooleanFilter(String propertyValue) {
        return propertyValue != null ? Boolean.valueOf(propertyValue) : null;
    }

    @Override
    public List<User> getListNullIfEmpty() {
        List<User> users = getList();
        return users.isEmpty() ? null : users;
    }

    @Override
    public User getUniqueResult() {
        if (getList().isEmpty()) throw new NullPointerException("No user found");
        if (getList().size() > 1) throw new ArrayIndexOutOfBoundsException("there is no unique result");
        return getList().get(0);
    }

    @Override
    public List<HQLStatistic<User>> getListWithStatistics(List<HQLStatisticItem> list) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    @Override
    public List<Object> getListWithStatisticsItems(List<HQLStatisticItem> list, HQLStatistic<User> hqlStatistic, int i) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    @Override
    public void setFirstResult(int offset) {
        this.offset = offset;
    }

    @Override
    public void setMaxResults(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean isCachable() {
        return true;
    }

    @Override
    public void setCachable(boolean b) {
        // Not implemented.
    }

    @Override
    public void addRestriction(HQLRestriction hqlRestriction) {
        throw new UnsupportedOperationException("Use addLike or addEq methods instead.");
    }

    @Override
    public Object getId(User gumUser) {
        return gumUser.getId();
    }

    @Override
    public Map<String, Long> getPossibleCountValues(String propertyName) {
        String search = getRestriction(SEARCH);
        String organizationId = getRestriction(ORGANIZATION_ID);
        String role = getRestriction(ROLES);
        Boolean activated = getBooleanFilter(getRestriction(ACTIVATED));
        return userService.getValueCount(propertyName, search, organizationId, role, activated);
    }
}
