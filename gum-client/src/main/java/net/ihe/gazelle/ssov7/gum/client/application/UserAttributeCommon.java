package net.ihe.gazelle.ssov7.gum.client.application;

public interface UserAttributeCommon {

    String getUserName(String userId);
}
