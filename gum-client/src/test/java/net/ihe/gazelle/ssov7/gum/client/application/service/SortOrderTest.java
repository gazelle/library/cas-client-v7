package net.ihe.gazelle.ssov7.gum.client.application.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortOrderTest {

    @Test
    public void getByStringOrDefaultTest() {
        assertEquals(SortOrder.DESC, SortOrder.getByStringOrDefault("DESC"));
        assertEquals(SortOrder.ASC, SortOrder.getByStringOrDefault("ASC"));
        assertEquals(SortOrder.ASC, SortOrder.getByStringOrDefault(""));
    }

}