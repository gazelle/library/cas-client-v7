package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import net.ihe.gazelle.ssov7.gum.client.utils.FakeJwtGenerator;
import net.ihe.gazelle.ssov7.m2m.client.interlay.AccessTokenRequestInterceptor;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import org.apache.http.HttpHeaders;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
//Ignore needed for WireMockRule to work
@PowerMockIgnore({"org.xml.*", "javax.net.ssl.*", "javax.xml.*", "java.xml.*", "javax.security.*", "com.github.tomakehurst.*"})
@PrepareForTest({M2MKeycloakConfigurationServiceImpl.class, AccessTokenRequestInterceptor.class})
public class SSOGumUserPreferencesClientTest {

    private static final String BEARER = "Bearer ";
    private static final String GUM_REST_USERS = "/gum/rest/users/";
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    UserPreferencesService userPreferencesService;
    private UserPreference baseUserPreference;
    private String token;

    private final byte[] profilePicture = "profilePicture".getBytes(StandardCharsets.UTF_8);
    private final byte[] thumbnailPicture = "thumbnailPicture".getBytes(StandardCharsets.UTF_8);

    @Before
    public void setUp() throws Exception {
        baseUserPreference = new UserPreference();
        baseUserPreference.setUserId("userId");
        baseUserPreference.setLanguagesSpoken(Arrays.asList("fr", "en"));
        baseUserPreference.setTableLabel("tableLabel");
        baseUserPreference.setNotifiedByEmail(true);
        String baseUrl = wireMockRule.baseUrl();
        environmentVariables.set("GUM_REST_API_URL", baseUrl + "/gum");

        M2MKeycloakConfigurationServiceImpl m2MConfigurationService = PowerMockito.mock(M2MKeycloakConfigurationServiceImpl.class);
        //Mocking all new instances of KeycloakConfigurationServiceImpl
        PowerMockito.whenNew(M2MKeycloakConfigurationServiceImpl.class)
                .withNoArguments()
                .thenReturn(m2MConfigurationService);
        when(m2MConfigurationService.getM2MClientId()).thenReturn("m2M-test-78459s");
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn(baseUrl);
        environmentVariables.set("GZL_M2M_CLIENT_SECRET", "secret");
        token = new FakeJwtGenerator().generateFakeJwt(System.currentTimeMillis() / 1000);
        wireMockRule.stubFor(post("/realms/gazelle/protocol/openid-connect/token")
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + token + "\"}")
                ));


        userPreferencesService = new SSOGumUserPreferencesClient();
    }

    @Test
    public void getUserPreferencesByUserId() {
        String id = baseUserPreference.getUserId();
        stubFor(get(GUM_REST_USERS + id + "/preferences")
                .willReturn(unauthorized()));
        stubFor(get(GUM_REST_USERS + id + "/preferences")
                .withHeader(HttpHeaders.AUTHORIZATION, matching(BEARER + token))
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference))));
        UserPreference actualUserPreference = userPreferencesService.getUserPreferencesByUserId(id);

        assertNotNull(actualUserPreference);
        verify(1, getRequestedFor(urlEqualTo(GUM_REST_USERS + id + "/preferences"))
                .withHeader(HttpHeaders.AUTHORIZATION, matching(BEARER + token)));
        assertEquals(baseUserPreference, actualUserPreference);
    }

    @Test(expected = NoSuchElementException.class)
    public void getUserPreferencesByUserIdNotFound() {
        String id = baseUserPreference.getUserId();
        stubFor(get(urlMatching("/gum/rest/users/.*"))
                .willReturn(aResponse().withStatus(404)));
        stubFor(get(GUM_REST_USERS + id + "/preferences")
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference))));
        userPreferencesService.getUserPreferencesByUserId("badId");
    }


    @Test
    public void getSingleUserPreferenceLanguagesSpoken() {
        String id = baseUserPreference.getUserId();
        stubFor(get(GUM_REST_USERS + id + "/preferences/languagesSpoken")
                .willReturn(aResponse()
                        .withBody(String.valueOf(generateJson(baseUserPreference.getLanguagesSpoken())))));
        assertEquals(baseUserPreference.getLanguagesSpoken(), userPreferencesService.getSingleUserPreference(id, "languagesSpoken"));

    }

    @Test
    public void getSingleUserPreferenceNotifiedByEmail() {
        String id = baseUserPreference.getUserId();
        stubFor(get(GUM_REST_USERS + id + "/preferences/notifiedByEmail")
                .willReturn(aResponse()
                        .withBody(String.valueOf(baseUserPreference.isNotifiedByEmail()))));
        assertEquals(baseUserPreference.isNotifiedByEmail(), userPreferencesService.getSingleUserPreference(id, "notifiedByEmail"));

    }

    @Test
    public void getSingleUserPreferencePictureAndThumbnail() {
        String id = baseUserPreference.getUserId();

        stubFor(get(GUM_REST_USERS + id + "/preferences/profilePicture")
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference.getProfilePictureUri()))));
        assertEquals(baseUserPreference.getProfilePictureUri(), userPreferencesService.getSingleUserPreference(id, "profilePicture"));

        stubFor(get(GUM_REST_USERS + id + "/preferences/profileThumbnail")
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference.getProfileThumbnailUri()))));
        assertEquals(baseUserPreference.getProfileThumbnailUri(), userPreferencesService.getSingleUserPreference(id, "profileThumbnail"));
    }

    @Test
    public void getSingleUserPreferenceTableLabel() {
        String id = baseUserPreference.getUserId();

        stubFor(get(GUM_REST_USERS + id + "/preferences/tableLabel")
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference.getTableLabel()))));
        assertEquals(baseUserPreference.getTableLabel(), userPreferencesService.getSingleUserPreference(id, "tableLabel"));
    }

    @Test(expected = NoSuchElementException.class)
    public void getSingleUserPreferenceNotFound() {
        String id = baseUserPreference.getUserId();
        //Here we stub everything to simulate the complete api.
        stubFor(get(GUM_REST_USERS + id + "/preferences/tableLabel").atPriority(1)
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference.getTableLabel()))));
        stubFor(get(GUM_REST_USERS + id + "/preferences/tableLabel").atPriority(1)
                .willReturn(aResponse()
                        .withBody(generateJson(baseUserPreference.getTableLabel()))));
        stubFor(get(GUM_REST_USERS + id + "/preferences/notifiedByEmail").atPriority(1)
                .willReturn(aResponse()
                        .withBody(String.valueOf(baseUserPreference.isNotifiedByEmail()))));
        stubFor(get(GUM_REST_USERS + id + "/preferences/languagesSpoken").atPriority(1)
                .willReturn(aResponse()
                        .withBody(String.valueOf(generateJson(baseUserPreference.getLanguagesSpoken())))));
        //If none of the stubs above matches the request, it falls back to the one below
        stubFor(get(urlMatching(GUM_REST_USERS + id + "/preferences/.*")).atPriority(10)
                .willReturn(notFound()));

        userPreferencesService.getSingleUserPreference(id, "badPreference");
    }

    @Test
    public void updateUserPreferences() {
        String id = baseUserPreference.getUserId();
        UserPreference userPreferenceUpdated = new UserPreference(baseUserPreference);
        userPreferenceUpdated.setLanguagesSpoken(Arrays.asList("de", "it"));
        userPreferenceUpdated.setTableLabel("A145");

        stubFor(put(GUM_REST_USERS + id + "/preferences")
                .willReturn(aResponse()
                        .withBody(generateJson(userPreferenceUpdated))));

        UserPreference actualUserPreference = userPreferencesService.updateUserPreferences(id, userPreferenceUpdated);

        assertNotNull(actualUserPreference);
        verify(1, putRequestedFor(urlEqualTo(GUM_REST_USERS + id + "/preferences"))
                .withRequestBody(equalToJson((generateJson(userPreferenceUpdated)))));
        assertNotEquals(baseUserPreference, actualUserPreference);
        assertEquals(userPreferenceUpdated, actualUserPreference);
    }

    @Test(expected = NoSuchElementException.class)
    public void updateUserPreferencesUserNotFound() {
        String id = baseUserPreference.getUserId();
        UserPreference userPreferenceUpdated = new UserPreference(baseUserPreference);
        userPreferenceUpdated.setLanguagesSpoken(Arrays.asList("de", "it"));
        userPreferenceUpdated.setTableLabel("A145");

        stubFor(put(urlMatching("/gum/rest/users/.*"))
                .willReturn(aResponse().withStatus(404)));
        stubFor(put(GUM_REST_USERS + id + "/preferences")
                .willReturn(aResponse()
                        .withBody(generateJson(userPreferenceUpdated))));

        userPreferencesService.updateUserPreferences("badId", userPreferenceUpdated);
    }

    @Test
    public void getUserProfilePictureTest() {
        String id = baseUserPreference.getUserId();
        stubFor(get(GUM_REST_USERS + id + "/preferences/picture?format=normal")
                .willReturn(ok().withBody(profilePicture).withHeader(HttpHeaders.CONTENT_TYPE, "image/jpeg")));

        byte[] userProfilePictureReturned = userPreferencesService.getUserProfilePicture(id, "normal");

        verify(1, getRequestedFor(urlEqualTo(GUM_REST_USERS + id + "/preferences/picture?format=normal")));
        assertNotNull(userProfilePictureReturned);
        assertArrayEquals(profilePicture, userProfilePictureReturned);
    }

    @Test
    public void getUserProfileThumbnailTest() {
        String id = baseUserPreference.getUserId();
        stubFor(get(GUM_REST_USERS + id + "/preferences/picture?format=thumbnail")
                .willReturn(ok().withBody(thumbnailPicture)));

        byte[] userProfileThumbnailReturned = userPreferencesService.getUserProfilePicture(id, "thumbnail");

        verify(1, getRequestedFor(urlEqualTo(GUM_REST_USERS + id + "/preferences/picture?format=thumbnail")));
        assertNotNull(userProfileThumbnailReturned);
        assertArrayEquals(thumbnailPicture, userProfileThumbnailReturned);
    }

    private String generateJson(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}