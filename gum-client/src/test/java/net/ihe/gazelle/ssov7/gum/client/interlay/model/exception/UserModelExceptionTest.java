package net.ihe.gazelle.ssov7.gum.client.interlay.model.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 18/09/2023
 */
public class UserModelExceptionTest extends UserModelException {

    private static final long serialVersionUID = 5221908873210651209L;
    private static final String message = "Test Exception";
    private final Throwable throwable = new Throwable(message);

    @Test
    public void testUserModelExceptionWithMessage() {
        UserModelException exception = new UserModelException(message);
        assertEquals(message, exception.getMessage());
    }

    @Test
    public void testUserModelExceptionWithThrowable() {
        UserModelException exception = new UserModelException(throwable);
        assertEquals(exception.getCause(), throwable);
    }

    @Test
    public void testUserModelExceptionSuper() {
        UserModelException exception = new UserModelException();
        assertNull(exception.getMessage());
    }

    @Test
    public void testUserModelExceptionWithMessageAndThrowable() {
        UserModelException exception = new UserModelException(message, throwable);
        assertEquals(exception.getCause(), throwable);
        assertEquals(message, exception.getMessage());
    }

}