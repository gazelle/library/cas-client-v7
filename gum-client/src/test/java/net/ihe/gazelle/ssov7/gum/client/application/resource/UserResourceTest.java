package net.ihe.gazelle.ssov7.gum.client.application.resource;

import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.ssov7.gum.client.interlay.ws.UserResource;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 07/10/2023
 */
public class UserResourceTest {

    @Test
    public void testUserResourceWithEmptyConstructor() {
        UserResource userResource = new UserResource();
        assertNull(userResource.getId());
        assertFalse(userResource.getConsent());
        assertNull(userResource.getEmail());
        assertNull(userResource.getPassword());
        assertNull(userResource.getOrganizationId());
        assertNull(userResource.getFirstName());
        assertNull(userResource.getLastName());
        assertNull(userResource.getOrganization());
        assertNull(userResource.getLastLoginTimestamp());
        assertNull(userResource.getLoginCounter());
        assertNull(userResource.getLastUpdateTimestamp());
        assertNull(userResource.getExternalId());
        assertNull(userResource.getIdpId());
    }

    @Test
    public void testUserResourceConstructWithIdParam() {
        UserResource user = new UserResource("UserTest");
        assertEquals("UserTest", user.getId());
    }

    @Test
    public void testUserResourceConstructWithParam() {
        UserResource user = new UserResource("John", "Doe", "KEREVAL", "admin_role", false);
        assertEquals("John", user.getFirstName());
        assertEquals("Doe", user.getLastName());
        assertTrue(user.getGroupIds().contains("admin_role"));
        assertEquals("KEREVAL", user.getOrganizationId());
        assertFalse(user.isActivated());
    }

    @Test
    public void testUserResourceConstructWithGumUser() {
        Set<String> groups = new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role"));
        User user = new User("user1", "John", "Doe", "john.doe@lost.com", groups, "KEREVAL", true, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        UserResource userResource = new UserResource(user);
        assertEquals("user1", userResource.getId());
        assertEquals("John", userResource.getFirstName());
        assertEquals("Doe", userResource.getLastName());
        assertEquals("john.doe@lost.com", userResource.getEmail());
        assertEquals(new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role")), userResource.getGroupIds());
        assertEquals("KEREVAL", userResource.getOrganizationId());
        assertTrue(userResource.isActivated());
        assertEquals(1601906470000L, userResource.getLastLoginTimestamp().getTime());
        assertEquals(0, userResource.getLoginCounter().intValue());
        assertEquals(1601906470000L, userResource.getLastUpdateTimestamp().getTime());
    }

    @Test
    public void testUserResourceConstructWithGumUserAndOrganisation() {
        OrganizationResource organizationResource = new OrganizationResource("KEREVAL", "Kereval", "Descritpion");
        Set<String> groups = new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role"));
        User user = new User("user1", "John", "Doe", "john.doe@lost.com", groups, "KEREVAL", true, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        UserResource userResource = new UserResource(user, organizationResource);
        assertEquals("user1", userResource.getId());
        assertEquals("John", userResource.getFirstName());
        assertEquals("Doe", userResource.getLastName());
        assertEquals("john.doe@lost.com", userResource.getEmail());
        assertEquals(new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role")), userResource.getGroupIds());
        assertEquals("KEREVAL", userResource.getOrganizationId());
        assertTrue(userResource.isActivated());
        assertEquals(1601906470000L, userResource.getLastLoginTimestamp().getTime());
    }

    @Test
    public void testUserResourceToStringWithConstructParam() {
        UserResource userResource = new UserResource("John", "Doe", "KEREVAL", "role:gazelle_admin", false);
        assertEquals("{firstName='John', lastName='Doe', email='null', groupIds=[role:gazelle_admin], " +
                "organizationId='KEREVAL', organization='null', password='null', passwordConfirmation='null', " +
                "activated=false, lastLoginTimestamp=null, loginCounter=null, lastUpdateTimestamp=null, consent=false, " +
                "externalId=null, idpId=null}", userResource.toString());
    }

    @Test
    public void testUserResourceToStringWithConstructWithGumUser() {
        Set<String> groups = new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role"));
        User user = new User("user1", "John", "Doe", "john.doe@lost.com", groups, "KEREVAL", true, null, 0, null);
        UserResource userResource = new UserResource(user);
        assertEquals("{firstName='John', lastName='Doe', email='john.doe@lost.com', groupIds=[vendor_role, vendor_admin_role], " +
                "organizationId='KEREVAL', organization='null', password='null', passwordConfirmation='null', activated=true, " +
                "lastLoginTimestamp=null, loginCounter=0, lastUpdateTimestamp=null, consent=false, " +
                "externalId=null, idpId=null}", userResource.toString());
    }

    @Test
    public void testUserResourceToStringWithConstructWithGumUserAndOrganisation() {
        OrganizationResource organizationResource = new OrganizationResource("KEREVAL", "Kereval", "Descritpion");
        Set<String> groups = new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role"));
        User user = new User("user1", "John", "Doe", "john.doe@lost.com", groups, "KEREVAL", true, null, 0, null);
        UserResource userResource = new UserResource(user, organizationResource);
        assertEquals("{firstName='John', lastName='Doe', email='john.doe@lost.com', groupIds=[vendor_role, vendor_admin_role], " +
                "organizationId='KEREVAL', organization='KEREVAL', password='null', passwordConfirmation='null', " +
                "activated=true, lastLoginTimestamp=null, loginCounter=0, lastUpdateTimestamp=null, consent=false, " +
                "externalId=null, idpId=null}", userResource.toString());
    }

    @Test
    public void testUserResourceAsUser() {
        OrganizationResource organizationResource = new OrganizationResource("KEREVAL", "Kereval", "Descritpion");
        Set<String> groups = new HashSet<>(Arrays.asList("vendor_role", "vendor_admin_role"));
        User user = new User("user1", "Michelle", "Pfeiffer", "michelle.pfeiffer@kereval.com", groups, "KEREVAL", true, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        UserResource userResource = new UserResource(user, organizationResource);
        User newUser = userResource.asUser();
        assertEquals(userResource.getFirstName(), newUser.getFirstName());
        assertEquals(userResource.getLastName(), newUser.getLastName());
        assertEquals(userResource.getEmail(), newUser.getEmail());
        assertEquals(userResource.getGroupIds().size(), newUser.getGroupIds().size());
        assertEquals(userResource.getOrganizationId(), newUser.getOrganizationId());
        assertEquals(userResource.isActivated(), newUser.getActivated());
    }

    @Test
    public void testAllSetters() {
        OrganizationResource organizationResource = new OrganizationResource("KEREVAL", "Kereval", "Descritpion");
        UserResource userResource = new UserResource();
        userResource.setActivated(true);
        userResource.setConsent(true);
        userResource.setEmail("michelle.pfeiffer@kereval.com");
        userResource.setOrganizationId("KEREVAL");
        userResource.setOrganization(organizationResource);
        userResource.setFirstName("Michelle");
        userResource.setLastName("Pfeiffer");
        userResource.setGroupIds(new HashSet<>(Arrays.asList(Role.VENDOR, Role.VENDOR_ADMIN)));
        userResource.setPassword("test");
        userResource.setPasswordConfirmation("test");
        userResource.setLastLoginTimestamp(new Timestamp(1601906470000L));
        userResource.setLoginCounter(0);
        userResource.setLastUpdateTimestamp(new Timestamp(1601906470000L));

        assertTrue(userResource.isActivated());
        assertTrue(userResource.getConsent());
        assertEquals("Michelle", userResource.getFirstName());
        assertEquals("Pfeiffer", userResource.getLastName());
        assertEquals("test", userResource.getPassword());
        assertEquals("test", userResource.getPasswordConfirmation());
        assertEquals(new HashSet<>(Arrays.asList(Role.VENDOR, Role.VENDOR_ADMIN)), userResource.getGroupIds());
        assertEquals("michelle.pfeiffer@kereval.com", userResource.getEmail());
        assertEquals(1601906470000L, userResource.getLastLoginTimestamp().getTime());
        assertEquals(organizationResource, userResource.getOrganization());
        assertEquals(0, userResource.getLoginCounter().intValue());
        assertEquals(1601906470000L, userResource.getLastUpdateTimestamp().getTime());
    }

    @Test
    public void testEquals_Symmetric1() {
        UserResource x = new UserResource("John", "Doe", "KEREVAL", "admin_role", false);
        UserResource y = new UserResource("John", "Doe", "KEREVAL", "admin_role", false);
        assertTrue(x.equals(y) && y.equals(x));
        assertEquals(x.hashCode(), y.hashCode());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(UserResource.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

}