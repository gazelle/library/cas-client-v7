package net.ihe.gazelle.ssov7.gum.client.application.resource;

import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrganizationResourceTest {

    @Test
    public void testOrganisationModel() {
        OrganizationResource organisation = new OrganizationResource("id", "name", "url");
        assertEquals("id", organisation.getId());
        assertEquals("name", organisation.getName());
        assertEquals("url", organisation.getUrl());
        organisation.setId("newId");
        organisation.setName("newName");
        organisation.setUrl("newUrl");
        organisation.setExternalId("externalId");
        organisation.setIdpId("idpId");
        assertEquals("newId", organisation.getId());
        assertEquals("newName", organisation.getName());
        assertEquals("newUrl", organisation.getUrl());
        assertEquals("externalId", organisation.getExternalId());
        assertEquals("idpId", organisation.getIdpId());
    }

    @Test
    public void testOrganizationResourceEmptyConstructor() {
        OrganizationResource organizationResource = new OrganizationResource();
        assertNull(organizationResource.getId());
        assertNull(organizationResource.getName());
        assertNull(organizationResource.getUrl());
    }

    @Test
    public void testEquals_Symmetric1() {
        OrganizationResource x = new OrganizationResource("id", "name", "url");
        OrganizationResource y = new OrganizationResource("id", "name", "url");
        assertTrue(x.equals(y) && y.equals(x));
        assertEquals(x.hashCode(), y.hashCode());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(OrganizationResource.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

    @Test
    public void testOrganizationResourceToStringWithConstructParam() {
        OrganizationResource organizationResource = new OrganizationResource("KEREVAL", "Kereval", "Url", "externalId", "idpId");
        assertEquals("OrganizationResource{id='KEREVAL', name='Kereval', url='Url', externalId='externalId', idpId='idpId'}", organizationResource.toString());
    }
}
