package net.ihe.gazelle.ssov7.gum.client.utils;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

public class FakeJwtGenerator {

    public String generateFakeJwt(long expiration) {
        String fake = DatatypeConverter.printBase64Binary
                (("{}").getBytes(StandardCharsets.UTF_8)) +
                "." +
                DatatypeConverter.printBase64Binary
                        (("{\"exp\":\"" + expiration + "\"}").getBytes(StandardCharsets.UTF_8)) +
                "." +
                DatatypeConverter.printBase64Binary
                        (("{}").getBytes(StandardCharsets.UTF_8));
        return fake.replaceAll("=", "");
    }
}
