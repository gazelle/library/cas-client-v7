package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.ihe.gazelle.ssov7.gum.client.application.service.GroupService;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import net.ihe.gazelle.ssov7.gum.client.utils.FakeJwtGenerator;
import net.ihe.gazelle.ssov7.m2m.client.interlay.AccessTokenRequestInterceptor;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
//Ignore needed for WireMockRule to work
@PowerMockIgnore({"org.xml.*", "javax.net.ssl.*", "javax.xml.*", "java.xml.*", "javax.security.*", "com.github.tomakehurst.*"})
@PrepareForTest({M2MKeycloakConfigurationServiceImpl.class, AccessTokenRequestInterceptor.class})
public class SSOGumGroupClientTest {

    private GroupService groupService;
    private HttpClient client;
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private String jsonGroups;


    @Before
    public void setUp() throws Exception {
        String baseUrl = wireMockRule.baseUrl();
        environmentVariables.set("GUM_REST_API_URL", baseUrl + "/gum");

        M2MKeycloakConfigurationServiceImpl m2MConfigurationService = PowerMockito.mock(M2MKeycloakConfigurationServiceImpl.class);
        //Mocking all new instances of KeycloakConfigurationServiceImpl
        PowerMockito.whenNew(M2MKeycloakConfigurationServiceImpl.class)
                .withNoArguments()
                .thenReturn(m2MConfigurationService);
        when(m2MConfigurationService.getM2MClientId()).thenReturn("m2M-test-78459s");
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn(baseUrl);
        environmentVariables.set("GZL_M2M_CLIENT_SECRET", "secret");
        client = mock(HttpClient.class);
        String token = new FakeJwtGenerator().generateFakeJwt(System.currentTimeMillis() / 1000);
        wireMockRule.stubFor(post("/realms/gazelle/protocol/openid-connect/token")
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + token + "\"}")
                ));

        client = mock(HttpClient.class);
        try {
            String role = "src/test/resources/groups.json";
            jsonGroups = readFileAsString(role);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        groupService = new SSOGumGroupsClient();
    }

    @Test
    public void testGetGroups() {
        stubFor(get("/gum/rest/groups").willReturn(aResponse().withBody(jsonGroups)));
        assertNotNull(groupService.getGroups());
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/groups")));
        assertEquals(2, groupService.getGroups().size());
    }

    @Test
    public void testGetGroupsThrowsGumSsoClientHttpErrorException() {
        HttpGet get = new HttpGet("http://localhost:8080/gum/rest/groups");
        stubFor(get("/gum/rest/groups").willReturn(null));
        try {
            when(client.execute(get)).thenThrow(new GumSsoClientHttpErrorException("Can't create client"));
            groupService.getGroups();
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("Can't create client", e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String readFileAsString(String file) throws Exception {
        return new String(Files.readAllBytes(Paths.get(file)), StandardCharsets.UTF_8);
    }

}
