package net.ihe.gazelle.ssov7.gum.client.utils;

import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserSearchParamsTest {

    @Test
    public void testNullParams() {
        UserSearchParams userSearchParams = new UserSearchParams();

        assertNull(userSearchParams.getSearch());
        assertNull(userSearchParams.getActivated());
        assertNull(userSearchParams.getDelegated());
        assertNull(userSearchParams.getEmail());
        assertNull(userSearchParams.getExternalId());
        assertNull(userSearchParams.getFirstName());
        assertNull(userSearchParams.getIdpId());
        assertNull(userSearchParams.getLastName());
        assertNull(userSearchParams.getLimit());
        assertNull(userSearchParams.getOffset());
        assertNull(userSearchParams.getOrganizationId());
        assertNull(userSearchParams.getGroup());
        assertNull(userSearchParams.getSortBy());
        assertNull(userSearchParams.getSortOrder());
    }

    @Test
    public void testSetParams() {
        UserSearchParams userSearchParams = new UserSearchParams()
                .setSearch("search")
                .setActivated(true)
                .setDelegated(false)
                .setEmail("email")
                .setExternalId("externalId")
                .setFirstName("firstName")
                .setIdpId("idpId")
                .setLastName("lastName")
                .setLimit(10)
                .setOffset(0)
                .setOrganizationId("organizationId")
                .setGroup("role")
                .setSortBy("sortBy")
                .setSortOrder(SortOrder.ASC);

        assertEquals("search", userSearchParams.getSearch());
        assertTrue(userSearchParams.getActivated());
        assertFalse(userSearchParams.getDelegated());
        assertEquals("email", userSearchParams.getEmail());
        assertEquals("externalId", userSearchParams.getExternalId());
        assertEquals("firstName", userSearchParams.getFirstName());
        assertEquals("idpId", userSearchParams.getIdpId());
        assertEquals("lastName", userSearchParams.getLastName());
        assertEquals(10, userSearchParams.getLimit().intValue());
        assertEquals(0, userSearchParams.getOffset().intValue());
        assertEquals("organizationId", userSearchParams.getOrganizationId());
        assertEquals("role", userSearchParams.getGroup());
        assertEquals("sortBy", userSearchParams.getSortBy());
        assertEquals(SortOrder.ASC, userSearchParams.getSortOrder());
    }
}
