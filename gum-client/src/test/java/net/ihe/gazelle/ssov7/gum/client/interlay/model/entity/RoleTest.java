package net.ihe.gazelle.ssov7.gum.client.interlay.model.entity;

import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Clément LAGORCE
 * @company KEREVAL
 * @project sso-client-v7
 * @date 25/10/2023
 */
public class RoleTest {

    @Test
    public void testGetRoleName() {
        assertEquals("admin_role", Role.ADMIN);
        assertEquals("monitor_role", Role.MONITOR);
        assertEquals("project-manager_role", Role.PROJECT_MANAGER);
        assertEquals("accounting_role", Role.ACCOUNTING);
        assertEquals("vendor_admin_role", Role.VENDOR_ADMIN);
        assertEquals("vendor_role", Role.VENDOR);
        assertEquals("user_role", Role.USER);
        assertEquals("tf_editor_role", Role.TF_EDITOR);
        assertEquals("tests_editor_role", Role.TESTS_EDITOR);
        assertEquals("institutions_editor_role", Role.INSTITUTIONS_EDITOR);
        assertEquals("vendor_late_registration_role", Role.VENDOR_LATE_REGISTRATION);
        assertEquals("testing_session_admin_role", Role.TESTING_SESSION_ADMIN);
    }

    @Test
    public void testValues() {
        assertEquals(12, Role.values().size());
    }

    @Test
    public void testGetterSetter() {
        Role role = new Role();
        role.setName(Role.ADMIN);
        assertEquals(Role.ADMIN, role.getName());
        role = new Role(Role.ADMIN);
        assertEquals(Role.ADMIN, role.getName());
        role.setDescription("the admin role");
        assertEquals("the admin role", role.getDescription());
        role = new Role(Role.ADMIN, "the admin role");
        assertEquals(Role.ADMIN, role.getName());
        assertEquals("the admin role", role.getDescription());
    }

    @Test
    public void testIsUserVendorAdminOK() {
        Set<String> groups = new HashSet<>(Arrays.asList("org-adm:groupId", "role:gazelle_admin"));
        User user = new User("user2", "John", "Doe", "john.doe@lost.com", groups, "groupId", false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertTrue(Role.isUserVendorAdmin(user));
        assertTrue(user.hasRole(Role.ADMIN));
    }

    @Test
    public void testIsUserVendorAdminKO() {
        Set<String> groups = new HashSet<>(Arrays.asList("admin_role", "user_role"));
        User user = new User("user2", "John", "Doe", "john.doe@lost.com", groups, "groupId", false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertFalse(Role.isUserVendorAdmin(user));
    }

    @Test
    public void testIsUserVendorAdminKOCauseNullUser() {
        assertFalse(Role.isUserVendorAdmin(null));
    }

}