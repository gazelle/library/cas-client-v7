# M2M v7

<!-- TOC -->
* [M2M v7](#m2m-v7)
* [M2M client](#m2m-client)
  * [Description](#description)
  * [Client registration](#client-registration)
  * [Get an access token](#get-an-access-token)
* [M2M server](#m2m-server)
  * [Description](#description-1)
  * [Public keys](#public-keys)
  * [JWT validator](#jwt-validator)
  * [Filter](#filter)
  * [Scopes registration](#scopes-registration)
  * [Environment variables](#environment-variables)
* [References](#references)
<!-- TOC -->


This module contains the necessary component needed to add Machine to Machine (M2M) secured communication to your project.
You will find component for M2M client and M2M server.

# M2M client

## Description

For M2M to works, the client must have an Authorization Bearer token attached to its requests. For that, you can use the
M2MHttpClientBuilder which is an extension of Apache HttpClientBuilder. It works the same way but has already an interceptor
that adds the Authorization Bearer with an already fetched JWT from Keycloak.

To fetch this JWT the interceptor uses the AccessTokenProvider with the KeycloakAccessTokenProvider implementation.

You must provide a list of scopes, it can be empty, that will be added to the JWT.

## Client registration

An application requesting an access token must be registered as client in Keycloak. This registration is handled by this 
module.

You will need the following environment variable for registration to work:

| Name                   | Description                                                     | Value example               |
|------------------------|-----------------------------------------------------------------|-----------------------------|
| GZL_SSO_URL            | The url of the server used for m2m (ex Keycloak)                | http://localhost:28080/auth |
| GZL_M2M_CLIENT_SECRET  | The secret of the Keycloak client corresponding the application | changeit                    |

## Get an access token

There is 3 ways to add the access token to your request as seen below (with example):
- Use the M2MHttpClientBuilder as a classical Apache HttpClientBuilder

```java
List<String> scopes = Collections.singletonList("my-scope");

try (CloseableHttpClient client = M2MHttpClientBuilder.createWithAccessToken(scopes).build()) {

} catch (IOException e) {
    throw new RuntimeException(e);
}
```

- Add the AccessTokenRequestInterceptor to your Apache HttpClient

```java
List<String> scopes = Collections.singletonList("my-scope");
try (CloseableHttpClient client = HttpClientBuilder.create()
        .addInterceptorFirst(new AccessTokenRequestInterceptor(scopes))
        .build()){

} catch (IOException e) {
    throw new RuntimeException(e);
}
```

- Use the AccessTokenProvider to add the token to your request :
```java
SSOConfigurationService ssoConfigurationService = new KeycloakConfigurationServiceImpl();
AccessTokenProvider accessTokenProvider = new KeycloakAccessTokenProvider(ssoConfigurationService);

List<String> scopes = Collections.singletonList("my-scope");
String accessToken = accessTokenProvider.getAccessToken("myClientID", "myClientSecret", scopes);
```

# M2M server

>:warning: **Warning:**
>
> This does not provide any server, it is only meant to be used server-side to provide M2M.


## Description

This module provides a PublicKeyProvider interface used to get a public key. This key will be used to verify the JWT
signature. For now there is 3 implementations, one for local key (.pem file), one for keystore and one to retrieve the key
using a Keycloak endpoint. The PublicKeyProviderFactory will select automatically which provider to use depending on
the environment variables you have provided (see [m2m section](#environment-variables)).

It also provides a JWTValidator that will allow to verify any JWT.

## Public keys

For the PublicKeyProviderFactory to choose the right provider, it relies on a PublicKeyConfig implementation.
The implementation provided in this module works like this :

- If the keystore alias and password variables ([see here](#environment-variables)) are set then it will suppose that the path provided
  is for a keystore.
- Else if the keystore alias and password are not set, and that the public path start with __*"http"*__ it will assume that your key is
  remote will fetch it with simple GET request.
- Finally, if no provider is selected before it will assume the public key is local and will try to find the key following the path
  provided.

## JWT validator

There are 5 elements that must be validated for a JWT to be accepted:
* The signature, to certifies that it comes from a trusted source.
* The issuer (iss claim), to verify who sent issued this token.
* The audience (aud claim), to verify if the token is destined for the current application.
* When it issued (iat claim), to verify when it was issued.
* The expiration time (exp claim), to verify if the token is still valid.

>:warning: **Warning:**
>
> If one of these elements is not validated, then the token is refused and the request aborted.


## Filter

Using the JWTValidatorFilter, you can protect your endpoints with M2M.

To add the filter your application, add the following in the web.xml file:

```xml
    <filter>
        <filter-name>JWTValidatorFilter</filter-name>
        <filter-class>net.ihe.gazelle.ssov7.m2m.server.interlay.filter.JWTValidatorFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>JWTValidatorFilter</filter-name>
        <url-pattern>/path/to/my/resource</url-pattern>
    </filter-mapping>
```

Also add this to your component.xml file:
```xml
<web:context-filter url-pattern="/media/*"/>
```

## Scopes registration

To limit what an application as access to, we use scopes. These scopes are in the JWT and will be verified by the resource
server needing them. It means that client application should know these scopes exist and for that we use Keycloak and its
__client scopes__. To allow access of a scope for applications, the Keycloak client of this application should have the
client scopes.

These client scopes are added when a resource server is deployed if it has this module as a dependency. For them to be 
added to Keycloak, you must implement the *ProvidedInterfaceProvider* interface in your project. More information see 
the [Metadata v7](https://gitlab.inria.fr/gazelle/public/framework/metadata-v7) readme. If no implementation is found 
then no scope will be added to Keycloak.

>:information_source: **Note:**
>
> Due to the current implementation, resource server will also be registered as client in Keycloak.
>
> See [Client registration](#client-registration) for more information.

## Environment variables

| Name                                       | Description                                                                                                                                       | Value example                                                            |
|--------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
| GZL_M2M_IDENTITY_EMAIL                     | The email of the identity used for m2m.                                                                                                           | indus@kereval.com                                                        |
| JWT_VERIFY_PUBLIC_KEY_LOCATION             | The location of the public key. Can be locally on your server, can be remote (must point to an array of JSON Web Keys) or can be a keystore file. | https://fakeurl.fake/keys , ./keys/myKey.pem, ./keystores/myKeyStore.jks |
| (Optional) JWT_VERIFY_TRUSTSTORE_KEY_ALIAS | The alias of the key to retrieve in your truststore.                                                                                              | myAlias                                                                  |
| (Optional) JWT_VERIFY_TRUSTSTORE_PASSWORD  | The password required to access your truststore.                                                                                                  |                                                                          |

# References

JSON Web Token (JWT): [https://datatracker.ietf.org/doc/html/rfc7519](https://datatracker.ietf.org/doc/html/rfc7519)
Context management for custom servlets (31.1.3.8): [https://docs.jboss.org/seam/latest/reference/html/configuration.html#d0e24317](https://docs.jboss.org/seam/latest/reference/html/configuration.html#d0e24317)
