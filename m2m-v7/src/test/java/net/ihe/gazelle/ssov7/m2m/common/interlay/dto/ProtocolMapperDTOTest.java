package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class ProtocolMapperDTOTest {


    @Test
    public void constructWithSettersTest(){
        ProtocolMapperDTO protocolMapperDTO = new ProtocolMapperDTO();
        protocolMapperDTO.setName("mapper");
        protocolMapperDTO.setProtocol("oidc");
        protocolMapperDTO.setId("id");
        protocolMapperDTO.setConfig(Collections.singletonMap("key", "value"));

        assertEquals("mapper",protocolMapperDTO.getName());
        assertEquals("oidc",protocolMapperDTO.getProtocol());
        assertEquals("id",protocolMapperDTO.getId());
        assertEquals(Collections.singletonMap("key", "value"),protocolMapperDTO.getConfig());

    }

    @Test
    public void equalsTest(){
        EqualsVerifier.forClass(RoleDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }

}