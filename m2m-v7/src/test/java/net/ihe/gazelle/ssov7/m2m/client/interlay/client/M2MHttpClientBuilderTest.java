package net.ihe.gazelle.ssov7.m2m.client.interlay.client;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.ihe.gazelle.ssov7.m2m.client.interlay.AccessTokenRequestInterceptor;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static net.ihe.gazelle.ssov7.m2m.client.interlay.client.KeycloakAccessTokenProvider.REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
//Ignore needed for WireMockRule to work
@PowerMockIgnore({"org.xml.*", "javax.net.ssl.*", "javax.xml.*", "java.xml.*", "javax.security.*", "com.github.tomakehurst.*"})
@PrepareForTest({M2MKeycloakConfigurationServiceImpl.class, AccessTokenRequestInterceptor.class})
public class M2MHttpClientBuilderTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(12345));
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @BeforeClass
    public static void setUpAll() throws Exception {
        M2MKeycloakConfigurationServiceImpl m2MConfigurationService = PowerMockito.mock(M2MKeycloakConfigurationServiceImpl.class);
        //Mocking all new instances of KeycloakConfigurationServiceImpl
        PowerMockito.whenNew(M2MKeycloakConfigurationServiceImpl.class)
                .withNoArguments()
                .thenReturn(m2MConfigurationService);
        when(m2MConfigurationService.getM2MClientId()).thenReturn("m2M-test-78459s");
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn("http://localhost:12345");
    }

    @Before
    public void setUp() {
        environmentVariables.set("GZL_M2M_CLIENT_SECRET", "secret");
    }

    @Test
    public void M2MHttpClientTest() throws IOException {

        String expectedAccessToken = "thisIsAMockedAccessToken";
        wireMockRule.stubFor(post(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + expectedAccessToken + "\"}")
                ));
        wireMockRule.stubFor(get("/secured").withHeader(HttpHeaders.AUTHORIZATION,
                        matching("Bearer " + expectedAccessToken))
                .willReturn(ok()));
        wireMockRule.stubFor(get("/secured").withHeader(HttpHeaders.AUTHORIZATION,
                        notMatching("Bearer " + expectedAccessToken))
                .willReturn(aResponse().withStatus(403)));

        try (CloseableHttpClient client = M2MHttpClientBuilder.createWithAccessToken().build()) {
            HttpGet getSecured = new HttpGet("http://localhost:12345/secured");
            try (CloseableHttpResponse response = client.execute(getSecured)) {
                assertEquals(200, response.getStatusLine().getStatusCode());
            }
        }
    }
}