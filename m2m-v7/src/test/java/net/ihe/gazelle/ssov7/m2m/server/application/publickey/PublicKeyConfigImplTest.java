package net.ihe.gazelle.ssov7.m2m.server.application.publickey;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PublicKeyConfigImplTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setup() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "localPublicKey.pem");
        environmentVariables.set("JWT_VERIFY_TRUSTSTORE_KEY_ALIAS", "alias");
        environmentVariables.set("JWT_VERIFY_TRUSTSTORE_PASSWORD", "password");
    }

    @Test
    public void publicKeyConfigTest() {
        PublicKeyConfig publicKeyConfig = new PublicKeyConfigImpl();
        assertEquals(System.getenv("JWT_VERIFY_PUBLIC_KEY_LOCATION"), publicKeyConfig.getPublicKeyPath());
        assertEquals(System.getenv("JWT_VERIFY_TRUSTSTORE_KEY_ALIAS"), publicKeyConfig.getKeystoreAlias());
        assertEquals(System.getenv("JWT_VERIFY_TRUSTSTORE_PASSWORD"), publicKeyConfig.getKeystorePassword());
    }

    @Test(expected = NullPointerException.class)
    public void publicKeyConfigTestLocationNull() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", null);

        PublicKeyConfig publicKeyConfig = new PublicKeyConfigImpl();
        assertEquals(System.getenv("JWT_VERIFY_PUBLIC_KEY_LOCATION"), publicKeyConfig.getPublicKeyPath());
        assertEquals(System.getenv("JWT_VERIFY_TRUSTSTORE_KEY_ALIAS"), publicKeyConfig.getKeystoreAlias());
        assertEquals(System.getenv("JWT_VERIFY_TRUSTSTORE_PASSWORD"), publicKeyConfig.getKeystorePassword());

        fail("Expected NullPointerException to be thrown");
    }
}
