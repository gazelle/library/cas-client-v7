package net.ihe.gazelle.ssov7.m2m.common.interlay;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class M2MKeycloakDTOMapperTest {


    private final M2MKeycloakDTOMapper mapper = new M2MKeycloakDTOMapper();
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        environmentVariables.set("GZL_M2M_CLIENT_SECRET", "changeit");
    }

    @Test
    public void createRealmRoleRepresentationJsonTest() {
        String roleName = "role";
        String expectedJson = "{\"name\":\"" + roleName + "\",\"clientRole\":false,\"containerId\":\"gazelle\"}";
        assertEquals(expectedJson, mapper.createRealmRoleRepresentationJson(roleName));
        //TODO move to m2m server
    }

    @Test(expected = IllegalArgumentException.class)
    public void createRealmRoleRepresentationJsonRoleNameNullTest() {
        //TODO move to m2m server

        mapper.createRealmRoleRepresentationJson(null);
        fail("Expected IllegalArgumentException to be thrown");
    }

    @Test
    public void createRealmRoleRepresentationJsonWithIdTest() {

        //TODO move to m2m server
        String roleName = "role";
        String roleId = "uu-id-role";
        String expectedJson = "{\"id\":\"" + roleId + "\",\"name\":\"" + roleName + "\",\"clientRole\":false,\"containerId\":\"gazelle\"}";
        assertEquals(expectedJson, mapper.createRealmRoleRepresentationJson(roleId, roleName));
    }

    @Test
    public void createM2mClientScopeRepresentationJsonTest() {
        String clientScopeName = "scope:test";
        String clientScopeDescription = "Description";
        String expectedJson = "{\"name\":\"" + clientScopeName + "\"," +
                "\"description\":\""+clientScopeDescription+"\","+
                "\"protocol\":\"openid-connect\"," +
                "\"attributes\":{\"include.in.token.scope\":\"true\"," +
                "\"display.on.consent.screen\":\"false\"}" +
                "}";
        assertEquals(expectedJson, mapper.createM2mClientScopeRepresentationJson(clientScopeName,clientScopeDescription));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createM2mClientScopeRepresentationJsonNameNullTest() {
        mapper.createM2mClientScopeRepresentationJson(null,"description");
        fail("Expected IllegalArgumentException to be thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createM2mClientScopeRepresentationJsonDescriptionNullTest() {
        mapper.createM2mClientScopeRepresentationJson("name", null);
        fail("Expected IllegalArgumentException to be thrown");
    }

    @Test
    public void createProtocolMapperAudienceRepresentationJsonTest() {
        String clientId = "clientId";
        String expectedJson = "{\"name\":\"audience\"," +
                "\"protocol\":\"openid-connect\"," +
                "\"protocolMapper\":\"oidc-audience-mapper\"," +
                "\"config\":{\"id.token.claim\":\"false\"," +
                "\"access.token.claim\":\"true\"," +
                "\"included.custom.audience\":\"" + clientId + "\"," +
                "\"userinfo.token.claim\":\"false\"}}";
        assertEquals(expectedJson, mapper.createProtocolMapperAudienceRepresentationJson(clientId));
    }

    @Test
    public void createProtocolMapperAudienceRepresentationJsonWithIdTest() {
        String clientId = "clientId";
        String protocolMapperId = "uu-id-protocolMapper";
        String expectedJson = "{" +
                "\"id\":\"" + protocolMapperId + "\"," +
                "\"name\":\"audience\"," +
                "\"protocol\":\"openid-connect\"," +
                "\"protocolMapper\":\"oidc-audience-mapper\"," +
                "\"config\":{\"id.token.claim\":\"false\"," +
                "\"access.token.claim\":\"true\"," +
                "\"included.custom.audience\":\"" + clientId + "\"," +
                "\"userinfo.token.claim\":\"false\"}}";
        assertEquals(expectedJson, mapper.createProtocolMapperAudienceRepresentationJson(clientId, protocolMapperId));
    }

    @Test
    public void createProtocolMapperAuthzMethodRepresentationJsonWithIdTest() {
        String protocolMapperId = "uu-id-protocolMapper";
        String expectedJson = "{" +
                "\"id\":\"" +protocolMapperId+"\","+
                "\"name\":\"authz_method\"," +
                "\"protocol\":\"openid-connect\"," +
                "\"protocolMapper\":\"oidc-hardcoded-claim-mapper\"," +
                "\"config\":{" +
                "\"id.token.claim\":\"false\"," +
                "\"access.token.claim\":\"true\"," +
                "\"claim.name\":\"authz_method\"," +
                "\"claim.value\":\"M2M\"," +
                "\"userinfo.token.claim\":\"false\"}" +
                "}";
        assertEquals(expectedJson, mapper.createProtocolMapperAuthzMethodRepresentationJson(protocolMapperId));
    }
    @Test
    public void createProtocolMapperAuthzMethodRepresentationJsonTest() {
        String expectedJson = "{" +
                "\"name\":\"authz_method\"," +
                "\"protocol\":\"openid-connect\"," +
                "\"protocolMapper\":\"oidc-hardcoded-claim-mapper\"," +
                "\"config\":{" +
                "\"id.token.claim\":\"false\"," +
                "\"access.token.claim\":\"true\"," +
                "\"claim.name\":\"authz_method\"," +
                "\"claim.value\":\"M2M\"," +
                "\"userinfo.token.claim\":\"false\"}" +
                "}";
        assertEquals(expectedJson, mapper.createProtocolMapperAuthzMethodRepresentationJson(null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createProtocolMapperAudienceRepresentationJsonNullTest() {
        String protocolMapperId = "uu-id-protocolMapper";

        mapper.createProtocolMapperAudienceRepresentationJson(null, protocolMapperId);
        fail("Expected IllegalArgumentException to be thrown");
    }

    @Test
    public void createClientRepresentationJsonTest() {
        String clientId = "m2m-clientId";
        String expectedJson = "{\"clientId\":\"" + clientId + "\"," +
                "\"protocol\":\"openid-connect\"," +
                "\"clientAuthenticatorType\":\"client-secret\"," +
                "\"name\":\"" + clientId + "\"," +
                "\"defaultClientScopes\":[\"m2m-client-scope\",\"microprofile-jwt\"]," +
                "\"authorizationServicesEnabled\":false," +
                "\"serviceAccountsEnabled\":true," +
                "\"publicClient\":false," +
                "\"standardFlowEnabled\":false," +
                "\"directAccessGrantsEnabled\":false," +
                "\"secret\":\"changeit\"}";

        assertEquals(expectedJson, mapper.createClientRepresentationJson(clientId));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClientRepresentationJsonClientIdNullTest() {

        mapper.createClientRepresentationJson(null);
        fail("Expected IllegalArgumentException to be thrown");

    }
}
