package net.ihe.gazelle.ssov7.m2m.client.interlay.dao;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientDAO;
import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientRegistrationService;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import net.ihe.gazelle.ssov7.m2m.mock.MetadataServiceProviderMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.ADMIN_REALMS_GAZELLE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class M2MClientDAOTest {

    public static final String AUTHORIZATION = "Authorization";
    private M2MClientDAO m2MClientDAO;
    private final int portNumber = Integer.parseInt(System.getProperty("testMockPort"));
    private static final String accessTokenMock = "thisIsAMockedAccessToken";
    private final MetadataServiceProvider metadataService = new MetadataServiceProviderMock();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(portNumber));
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        M2MConfigurationService m2MConfigurationService = mock(M2MKeycloakConfigurationServiceImpl.class);
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn("http://localhost:" + portNumber);
        m2MClientDAO = new M2MClientDAOImpl(m2MConfigurationService);

        wireMockRule.stubFor(post(M2MClientRegistrationService.REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        environmentVariables.set("GZL_M2M_CLIENT_SECRET","changeit");
    }

    @Test
    public void getRealmRoleIdTest() {
        String roleName = "roleName";
        String roleId = "roleId";
        wireMockRule.stubFor(get(ADMIN_REALMS_GAZELLE + "/roles?name=" + roleName)
                .willReturn(ok().withBody("[{\"id\":\"" + roleId + "\",\"name\":\"" + roleName + "\"}]")));

        assertEquals(roleId, m2MClientDAO.getRealmRoleId(roleName));
        verify(getRequestedFor(urlEqualTo(ADMIN_REALMS_GAZELLE + "/roles?name=" + roleName))
                .withHeader(AUTHORIZATION, containing(accessTokenMock)));
    }



    @Test
    public void addOptionalClientScopeToClientTest() {
        String idOfClient = "idOfClient";
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + "/clients/" + idOfClient + "/optional-client-scopes/" + clientScopeId;

        wireMockRule.stubFor(put(url)
                .willReturn(ok()));

        m2MClientDAO.addClientScopeToClient(idOfClient, clientScopeId,true);
        verify(getMethodWithAuthorization(RequestMethod.PUT, url));
    }

    @Test
    public void addDefaultClientScopeToClientTest() {
        String idOfClient = "idOfClient";
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + "/clients/" + idOfClient + "/default-client-scopes/" + clientScopeId;

        wireMockRule.stubFor(put(url)
                .willReturn(ok()));

        m2MClientDAO.addClientScopeToClient(idOfClient, clientScopeId,false);
        verify(getMethodWithAuthorization(RequestMethod.PUT, url));
    }

    @Test
    public void getIdOfClientTest() {
        String clientId = "clientId";
        String url = ADMIN_REALMS_GAZELLE + "/clients?clientId=" + clientId;
        String idOfClient = "idOfClient";
        wireMockRule.stubFor(get(url)
                .willReturn(ok()
                        .withBody("[{\"id\":\"" + idOfClient + "\",\"clientId\":\"" + clientId + "\"}]")));
        assertEquals(idOfClient, m2MClientDAO.getIdOfClient(clientId));
        verify(getMethodWithAuthorization(RequestMethod.GET, url));
    }

    @Test
    public void createClientTest() {
        String url = ADMIN_REALMS_GAZELLE + "/clients";
        String clientId = "clientId";

        wireMockRule.stubFor(post(url)
                .willReturn(created()));
        m2MClientDAO.createClient(clientId);
        verify(getRegisterClientRequestPatternBuilder(RequestMethod.POST,url,clientId));
    }

    @Test
    public void updateClientTest() {
        String idOfClient= "idOfClient";
        String url = ADMIN_REALMS_GAZELLE + "/clients/" + idOfClient;
        wireMockRule.stubFor(put(url)
                .willReturn(ok()));

        String clientId = "clientId";
        m2MClientDAO.updateClient(idOfClient,clientId);
        verify(getRegisterClientRequestPatternBuilder(RequestMethod.PUT,url,clientId));
    }

    @Test
    public void grantRoleToClientTest() {
        String idOfClient= "idOfClient";
        String serviceAccountUrl = ADMIN_REALMS_GAZELLE + "/clients/" + idOfClient + "/service-account-user";

        String serviceAccountId = "serviceAccountsId";

        wireMockRule.stubFor(get(serviceAccountUrl)
                .willReturn(ok().withBody("{\"id\":\""+serviceAccountId+"\"}")));
        String grantRoleUrl = "/admin/realms/gazelle/users/" + serviceAccountId + "/role-mappings/realm";

        String roleId="roleId";
        String roleName = "roleName";
        wireMockRule.stubFor(post(grantRoleUrl).willReturn(ok()));
        m2MClientDAO.grantRoleToClient(idOfClient,roleId, roleName);
        verify(getMethodWithAuthorization(RequestMethod.POST,grantRoleUrl));
    }

    private RequestPatternBuilder getRegisterClientRequestPatternBuilder(RequestMethod requestMethod, String url, String clientId) {
        return  getMethodWithAuthorization(requestMethod, url)
                .withRequestBody(matchingJsonPath("clientId", containing(clientId)))
                .withRequestBody(matchingJsonPath("protocol", containing("openid-connect")))
                .withRequestBody(matchingJsonPath("clientAuthenticatorType", containing("client-secret")))
                .withRequestBody(matchingJsonPath("secret", containing("changeit")))
                .withRequestBody(matchingJsonPath("serviceAccountsEnabled", containing("true")))
                .withRequestBody(matchingJsonPath("standardFlowEnabled", containing("false")))
                .withRequestBody(matchingJsonPath("directAccessGrantsEnabled", containing("false")))
                .withRequestBody(matchingJsonPath("publicClient", containing("false")));
    }
    private RequestPatternBuilder getMethodWithAuthorization(RequestMethod requestMethod, String url) {
        return new RequestPatternBuilder(requestMethod, (urlEqualTo(url)))
                .withHeader("Authorization", containing(accessTokenMock));
    }

}
