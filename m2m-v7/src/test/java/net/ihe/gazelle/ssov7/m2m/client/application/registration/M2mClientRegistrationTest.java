package net.ihe.gazelle.ssov7.m2m.client.application.registration;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakDTOMapperTest;
import net.ihe.gazelle.ssov7.m2m.client.interlay.dao.M2MClientDAOImpl;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import net.ihe.gazelle.ssov7.m2m.mock.MetadataServiceProviderMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;
import static net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientRegistrationService.REALM;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.xml.*", "javax.net.ssl.*", "javax.xml.*", "java.xml.*", "javax.security.*", "com.github" +
      ".tomakehurst.*"})
@PrepareForTest({M2MKeycloakDTOMapperTest.class})
public class M2mClientRegistrationTest {
   private static final int PORT_NUMBER = Integer.parseInt(System.getProperty("testMockPort"));
   private static final String accessTokenMock = "thisIsAMockedAccessToken";
   private M2MClientRegistrationService m2mClientRegistration;
   private final MetadataServiceProvider metadataService = new MetadataServiceProviderMock();

   @Rule
   public WireMockRule wireMockRule = new WireMockRule(options().port(PORT_NUMBER));
   @Rule
   public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
   private String clientId;

   private static M2MClientDAO m2MClientDAO;
   private static final String MACHINE_ROLE = M2MClientRegistrationService.MACHINE_ROLE;

   @BeforeClass
   public static void setUpAll() throws Exception {
      M2MKeycloakConfigurationServiceImpl m2MConfigurationService = PowerMockito.mock(
            M2MKeycloakConfigurationServiceImpl.class);
      //Mocking all new instances of KeycloakConfigurationServiceImpl
      PowerMockito.whenNew(M2MKeycloakConfigurationServiceImpl.class)
            .withNoArguments()
            .thenReturn(m2MConfigurationService);
      when(m2MConfigurationService.getM2MClientId()).thenReturn("m2M-test-78459s");
      when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn("http://localhost:" + PORT_NUMBER);
      m2MClientDAO = new M2MClientDAOImpl(m2MConfigurationService);
   }

   @Before
   public void setUp() {
      clientId = "m2m-" + metadataService.getMetadata().getName() + "-" + metadataService.getMetadata().getInstanceId();
      wireMockRule.stubFor(post("/realms/master/protocol/openid-connect/token")
            .willReturn(ok()
                  .withHeader("Content-type", "net/ihe/gazelle/application/json")
                  .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
            ));

      environmentVariables.set("GZL_M2M_CLIENT_SECRET", "changeit");

      m2mClientRegistration = new M2MClientRegistrationService(
            m2MClientDAO);
   }

   @Test
   public void registerClientWhenClientIdNotUniqueTest() {
      wireMockRule.stubFor(get(M2MClientRegistrationService.CLIENTS_URL + "?clientId=" + clientId)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idTest\"," +
                        "\"clientId\":\"" + clientId + "\"}]")
            ));
      wireMockRule.stubFor(get("/admin/realms/gazelle/roles?name=" + MACHINE_ROLE)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idRole\","
                        + "\"name\":\"" + MACHINE_ROLE + "\","
                        + "\"containerId\":\"" + REALM + "\""
                        + "}]")
            ));
      wireMockRule.stubFor(get("/admin/realms/gazelle/roles?name=role%3Agazelle_admin")
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idRole\","
                        + "\"name\":\"admin_role\","
                        + "\"containerId\":\"" + REALM + "\""
                        + "}]")
            ));
      wireMockRule.stubFor(get(M2MClientRegistrationService.CLIENTS_URL + "/idTest/service-account-user")
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("{\"id\":\"idServiceAccount\", \"name\":\"service-account\"}")
            ));

      wireMockRule.stubFor(get("/admin/realms/" + REALM + "/client-scopes?name=microprofile-jwt")
              .willReturn(ok()
                      .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                      .withBody("[{\"id\":\"microprofile-jwt-id\", \"name\":\"microprofile-jwt\"}]")));

      wireMockRule.stubFor(get("/admin/realms/" + REALM + "/client-scopes?name=m2m-client-scope")
              .willReturn(ok()
                      .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                      .withBody("[{\"id\":\"m2m-client-scope-id\", \"name\":\"m2m-client-scope\"}]")));

      wireMockRule.stubFor(put(M2MClientRegistrationService.CLIENTS_URL + "/idTest/default-client-scopes/m2m-client-scope-id")
              .willReturn(ok()));
      wireMockRule.stubFor(put(M2MClientRegistrationService.CLIENTS_URL + "/idTest/default-client-scopes/microprofile-jwt-id")
              .willReturn(ok()));

      wireMockRule.stubFor(post("/admin/realms/gazelle/users/idServiceAccount/role-mappings/realm")
            .willReturn(ok()));
      wireMockRule.stubFor(put(M2MClientRegistrationService.CLIENTS_URL + "/idTest")
            .willReturn(ok()));


      m2mClientRegistration.registerClient(clientId, "");
      verify(getRequestedFor(urlEqualTo(M2MClientRegistrationService.CLIENTS_URL + "?clientId=" + clientId))
            .withHeader("Authorization", containing(accessTokenMock)));

      verify(getRegisterClientRequestPatternBuilder(RequestMethod.PUT, "/admin/realms/gazelle/clients/idTest"));
   }

   @Test
   public void registerNewClient() {
      String registerNewClientScenario = "register new client";
      String clientRegistered = "client registered";

      wireMockRule.stubFor(get(M2MClientRegistrationService.CLIENTS_URL + "?clientId=" + clientId)
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(STARTED)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[]"))
      );
      wireMockRule.stubFor(post(M2MClientRegistrationService.CLIENTS_URL)
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(STARTED)
            .willReturn(ok())
            .willSetStateTo(clientRegistered)
      );
      wireMockRule.stubFor(get(M2MClientRegistrationService.CLIENTS_URL + "?clientId=" + clientId)
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(clientRegistered)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idTest\"," +
                        "\"clientId\":\"" + clientId + "\"}]")
            ));
      wireMockRule.stubFor(get("/admin/realms/gazelle/roles?name=" + MACHINE_ROLE)
            .inScenario(registerNewClientScenario)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idRole\","
                        + "\"name\":\"" + MACHINE_ROLE + "\","
                        + "\"containerId\":\"" + REALM + "\""
                        + "}]")
            ));
      wireMockRule.stubFor(get("/admin/realms/gazelle/roles?name=role%3Agazelle_admin")
            .inScenario(registerNewClientScenario)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("[{\"id\":\"idRole\","
                        + "\"name\":\"admin_role\","
                        + "\"containerId\":\"" + REALM + "\""
                        + "}]")
            ));
      wireMockRule.stubFor(get(M2MClientRegistrationService.CLIENTS_URL + "/idTest/service-account-user")
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(clientRegistered)
            .willReturn(ok()
                  .withHeader("Content-Type", "net/ihe/gazelle/application/json")
                  .withBody("{\"id\":\"idServiceAccount\","
                        + "\"name\":\"service-account\"}")
            ));

      wireMockRule.stubFor(post("/admin/realms/gazelle/users/idServiceAccount/role-mappings/realm")
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(clientRegistered)
            .willReturn(ok()));
      wireMockRule.stubFor(put(M2MClientRegistrationService.CLIENTS_URL + "/idTest")
            .inScenario(registerNewClientScenario)
            .whenScenarioStateIs(clientRegistered)
            .willReturn(ok()));

      m2mClientRegistration.registerClient(clientId, "http://localhost:8080/");
      verify(getRequestedFor(
            urlEqualTo(M2MClientRegistrationService.CLIENTS_URL + "?clientId=" + clientId))
            .withHeader("Authorization", containing(accessTokenMock))
      );
      verify(getRegisterClientRequestPatternBuilder(
            RequestMethod.POST, "/admin/realms/gazelle/clients")
      );

   }

   private RequestPatternBuilder getRegisterClientRequestPatternBuilder(RequestMethod requestMethod, String url) {
      return new RequestPatternBuilder(requestMethod, (urlEqualTo(url)))
            .withHeader("Authorization", containing(accessTokenMock))
            .withRequestBody(matchingJsonPath("clientId", containing(clientId)))
            .withRequestBody(matchingJsonPath("protocol", containing("openid-connect")))
            .withRequestBody(matchingJsonPath("clientAuthenticatorType", containing("client-secret")))
            .withRequestBody(matchingJsonPath("secret", containing("changeit")))
            .withRequestBody(matchingJsonPath("serviceAccountsEnabled", containing("true")))
            .withRequestBody(matchingJsonPath("standardFlowEnabled", containing("false")))
            .withRequestBody(matchingJsonPath("directAccessGrantsEnabled", containing("false")))
            .withRequestBody(matchingJsonPath("publicClient", containing("false")));
   }
}