package net.ihe.gazelle.ssov7.m2m.server.interlay.dao;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientRegistrationService;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import net.ihe.gazelle.ssov7.m2m.mock.MetadataServiceProviderMock;
import net.ihe.gazelle.ssov7.m2m.server.application.registration.M2MServerDAO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.ADMIN_REALMS_GAZELLE;
import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.CLIENT_SCOPES;
import static net.ihe.gazelle.ssov7.m2m.server.interlay.dao.M2MServerDAOImpl.PROTOCOL_MAPPERS_MODELS;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class M2MServerDAOTest {
    public static final String AUTHORIZATION = "Authorization";
    private M2MServerDAO m2MServerDAO;
    private final int portNumber = Integer.parseInt(System.getProperty("testMockPort"));
    private static final String accessTokenMock = "thisIsAMockedAccessToken";
    private final MetadataServiceProvider metadataService = new MetadataServiceProviderMock();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(portNumber));
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        M2MConfigurationService m2MConfigurationService = mock(M2MKeycloakConfigurationServiceImpl.class);
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn("http://localhost:" + portNumber);
        m2MServerDAO = new M2MServerDAOImpl(m2MConfigurationService);

        wireMockRule.stubFor(post(M2MClientRegistrationService.REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        environmentVariables.set("GZL_M2M_CLIENT_SECRET","changeit");
    }

    @Test
    public void createClientScopeTest() {
        String clientScopeName = "clientScopeName";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES;
        wireMockRule.stubFor(post(url)
                .willReturn(created()));

        String description = "this a description";
        m2MServerDAO.createClientScope(clientScopeName, description);
        verify(clientScopeRequestPatternBuilder(RequestMethod.POST, url, clientScopeName, description));
    }

    @Test
    public void getClientScopeIdTest() {
        String clientScopeName = "clientScopeName";
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "?name=" + clientScopeName;
        wireMockRule.stubFor(get(url)
                .willReturn(ok().withBody("[{\"id\":\"" + clientScopeId + "\",\"name\":\"" + clientScopeName + "\"}]")));

        assertEquals(clientScopeId, m2MServerDAO.getClientScopeId(clientScopeName));
        verify(getRequestedFor(urlEqualTo(url))
                .withHeader(AUTHORIZATION, containing(accessTokenMock)));
    }

    @Test
    public void updateClientScopeTest() {
        String clientScopeName = "clientScopeName";
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId;
        wireMockRule.stubFor(put(url)
                .willReturn(ok()));

        String description = "this a description";
        m2MServerDAO.updateClientScope(clientScopeId, clientScopeName, description);
        verify(clientScopeRequestPatternBuilder(RequestMethod.PUT, url, clientScopeName, description));
    }


    @Test
    public void scopeMappingOfRoleExistsTest() {
        String clientScopeId = "clientScopeId";
        String roleId = "roleId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + "/scope-mappings/realm?id=" + roleId;

        wireMockRule.stubFor(get(url)
                .willReturn(ok().withBody("[{\"id\":\"" + roleId + "\"}]")));
        assertTrue(m2MServerDAO.scopeMappingOfRoleExists(clientScopeId, roleId));
        verify(getMethodWithAuthorization(RequestMethod.GET, url));
    }

    @Test
    public void scopeMappingOfRoleNotExistsTest() {
        String clientScopeId = "clientScopeId";
        String roleId = "roleId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + "/scope-mappings/realm?id=" + roleId;

        wireMockRule.stubFor(get(url)
                .willReturn(ok().withBody("[{\"id\":\"notRoleId\"}]")));
        assertFalse(m2MServerDAO.scopeMappingOfRoleExists(clientScopeId, roleId));
        verify(getMethodWithAuthorization(RequestMethod.GET, url));
    }

    @Test
    public void createScopeMappingOfRoleTest() {
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + "/scope-mappings/realm";

        wireMockRule.stubFor(post(url)
                .willReturn(created()));

        String roleId = "roleId";
        String roleName = "roleName";
        m2MServerDAO.createScopeMappingOfRole(clientScopeId, roleId, roleName);

        verify(getMethodWithAuthorization(RequestMethod.POST, url));
    }

    @Test
    public void createProtocolMapperAudienceTest() {
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS;

        wireMockRule.stubFor(post(url)
                .willReturn(created()));
        m2MServerDAO.createProtocolMapperAudience(clientScopeId, clientScopeId);
        verify(protocolMapperRequestPatternBuilder(RequestMethod.POST, url, "audience", "oidc-audience-mapper"));
    }

    @Test
    public void updateProtocolMapperAudienceTest() {
        String clientScopeId = "clientScopeId";
        String audience = "audienceName";
        String protocolMapperAudienceId = "protocolMapperId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS + "/" + protocolMapperAudienceId;

        // Mock put url with body contains audience
        wireMockRule.stubFor(put(url)
                .withRequestBody(matchingJsonPath("config", containing(audience)))
                .willReturn(ok()));
        m2MServerDAO.updateProtocolMapperAudience(protocolMapperAudienceId, clientScopeId, audience);

        verify(protocolMapperRequestPatternBuilder(RequestMethod.PUT, url, "audience", "oidc-audience-mapper"));
    }

    @Test
    public void createProtocolMapperAuthzMethodTest() {
        String clientScopeId = "clientScopeId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + PROTOCOL_MAPPERS_MODELS;

        wireMockRule.stubFor(post(url)
                .willReturn(created()));
        m2MServerDAO.createProtocolMapperAuthzMethod(clientScopeId);
        verify(protocolMapperRequestPatternBuilder(
                RequestMethod.POST, url, "authz_method", "oidc-hardcoded-claim-mapper"));
    }

    @Test
    public void updateProtocolMapperAuthzMethodTest() {
        String clientScopeId = "clientScopeId";
        String protocolMapperAudienceId = "protocolMapperId";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS + "/" + protocolMapperAudienceId;
        wireMockRule.stubFor(put(url)
                .willReturn(ok()));
        m2MServerDAO.updateProtocolMapperAuthzMethod(protocolMapperAudienceId, clientScopeId);
        verify(protocolMapperRequestPatternBuilder(
                RequestMethod.PUT, url, "authz_method", "oidc-hardcoded-claim-mapper"));
    }

    @Test
    public void getProtocolMapperIdTest() {
        String clientScopeId = "clientScopeId";
        String mapperName = "audience";
        String url = ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS + "?name=" + mapperName;

        String mapperId = "mapperId";
        wireMockRule.stubFor(get(url)
                .willReturn(ok()
                        .withBody("[{\"id\":\"" + mapperId + "\",\"name\":\"" + mapperName + "\"}]")));
        assertEquals(mapperId, m2MServerDAO.getProtocolMapperId(clientScopeId, mapperName));
        verify(getMethodWithAuthorization(RequestMethod.GET, url));
    }

    @Test
    public void getRealmRoleIdTest() {
        String roleName = "roleName";
        String roleId = "roleId";
        wireMockRule.stubFor(get(ADMIN_REALMS_GAZELLE + "/roles?name=" + roleName)
                .willReturn(ok().withBody("[{\"id\":\"" + roleId + "\",\"name\":\"" + roleName + "\"}]")));

        assertEquals(roleId, m2MServerDAO.getRealmRoleId(roleName));
        verify(getRequestedFor(urlEqualTo(ADMIN_REALMS_GAZELLE + "/roles?name=" + roleName))
                .withHeader(AUTHORIZATION, containing(accessTokenMock)));
    }


    private RequestPatternBuilder clientScopeRequestPatternBuilder(RequestMethod requestMethod, String url,
                                                                   String clientScope, String description) {
        return getMethodWithAuthorization(requestMethod, url)
                .withRequestBody(matchingJsonPath("name", containing(clientScope)))
                .withRequestBody(matchingJsonPath("description", containing(description)))
                .withRequestBody(matchingJsonPath("protocol", containing("openid-connect")));
    }

    private RequestPatternBuilder protocolMapperRequestPatternBuilder(RequestMethod requestMethod, String url,
                                                                      String name, String protocolMapperType) {
        return getMethodWithAuthorization(requestMethod, url)
                .withRequestBody(matchingJsonPath("name", containing(name)))
                .withRequestBody(matchingJsonPath("protocolMapper", containing(protocolMapperType)))
                .withRequestBody(matchingJsonPath("protocol", containing("openid-connect")));
    }

    private RequestPatternBuilder getMethodWithAuthorization(RequestMethod requestMethod, String url) {
        return new RequestPatternBuilder(requestMethod, (urlEqualTo(url)))
                .withHeader("Authorization", containing(accessTokenMock));
    }
}
