package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class OidcKeycloakClientDTOTest {

    @Test
    public void constructWithSetterTest() {
        OidcKeycloakClientDTO oidcKeycloakClientDTO = new OidcKeycloakClientDTO();
        oidcKeycloakClientDTO.setClientId("id");
        oidcKeycloakClientDTO.setClientAuthenticatorType("authType");
        oidcKeycloakClientDTO.setName("name");
        oidcKeycloakClientDTO.setBaseUrl("http://baseurl.base");
        oidcKeycloakClientDTO.setDefaultClientScopes(Collections.singletonList("default"));
        oidcKeycloakClientDTO.setOptionalClientScopes(Collections.singletonList("optional"));
        oidcKeycloakClientDTO.setRedirectUris(Collections.singletonList("http://redirect.com"));
        oidcKeycloakClientDTO.setProtocolMappers(Collections.singletonList("protocol"));
        oidcKeycloakClientDTO.setPublicClient(false);
        oidcKeycloakClientDTO.setAuthorizationServicesEnabled(false);
        oidcKeycloakClientDTO.setDirectAccessGrantsEnabled(false);
        oidcKeycloakClientDTO.setStandardFlowEnabled(false);
        oidcKeycloakClientDTO.setServiceAccountsEnabled(true);
        assertEquals("id", oidcKeycloakClientDTO.getClientId());
        assertEquals("openid-connect",oidcKeycloakClientDTO.getProtocol());
        assertEquals("authType", oidcKeycloakClientDTO.getClientAuthenticatorType());
        assertEquals("name", oidcKeycloakClientDTO.getName());
        assertEquals("http://baseurl.base", oidcKeycloakClientDTO.getBaseUrl());
        assertEquals(Collections.singletonList("default"), oidcKeycloakClientDTO.getDefaultClientScopes());
        assertEquals(Collections.singletonList("optional"), oidcKeycloakClientDTO.getOptionalClientScopes());
        assertEquals(Collections.singletonList("protocol"), oidcKeycloakClientDTO.getProtocolMappers());
        assertFalse(oidcKeycloakClientDTO.isPublicClient());
        assertFalse(oidcKeycloakClientDTO.isDirectAccessGrantsEnabled());
        assertFalse(oidcKeycloakClientDTO.isStandardFlowEnabled());
        assertFalse(oidcKeycloakClientDTO.isAuthorizationServicesEnabled());
        assertTrue(oidcKeycloakClientDTO.isServiceAccountsEnabled());
    }

    @Test
    public void equalsTest(){
        EqualsVerifier.forClass(OidcKeycloakClientDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .withRedefinedSuperclass().verify();
    }

}