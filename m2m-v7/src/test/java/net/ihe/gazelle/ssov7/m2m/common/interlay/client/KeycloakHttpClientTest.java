package net.ihe.gazelle.ssov7.m2m.common.interlay.client;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class KeycloakHttpClientTest {

    private KeycloakHttpClient keycloakHttpClient;
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(
          Integer.parseInt(System.getProperty("testMockPort"))
    ));

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setup() {
        keycloakHttpClient = new KeycloakHttpClient(wireMockRule.baseUrl());

        environmentVariables.set("GZL_SSO_ADMIN_USER", "admin");
        environmentVariables.set("GZL_SSO_ADMIN_PASSWORD", "admin");

    }

    @Test
    public void executeRequestWithJsonBodyTest() {
        String accessTokenMock = "mocked access token";
        wireMockRule.stubFor(post(KeycloakHttpClient.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        try {
            wireMockRule.stubFor(post("/rest/post").willReturn(ok()));
            String jsonBody = "{\"message\":\"this is a message body\"}";
            HttpPost httpPost = new HttpPost(wireMockRule.baseUrl() + "/rest/post");
            keycloakHttpClient.executeRequestWithJsonBody(httpPost, jsonBody);

            verify(postRequestedFor(urlMatching("/rest/post")).withRequestBody(containing(jsonBody)));
        } catch (Exception e) {
            fail("Method should no throw exception when there is no HTTP error.\n" + e.getMessage());
        }
    }

    @Test
    public void getIdBySingleQueryParamArrayValueResponseTest() {
        String accessTokenMock = "mocked access token";
        wireMockRule.stubFor(post(KeycloakHttpClient.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        try {
            String myName = "myName";
            String myId = "myId";
            String jsonBody ="[{" +
                    "\"id\":\"" + myId + "\"," +
                    "\"name\":\"" + myName + "\"," +
                    "\"message\":\"this is a message body\"" +
                    "}]";
            wireMockRule.stubFor(get("/rest/get?name="+myName).willReturn(ok().withBody(jsonBody)));
            String url = wireMockRule.baseUrl() + "/rest/get";

            assertEquals(myId,keycloakHttpClient.getIdBySingleQueryParam("name", myName, url));

            verify(1,getRequestedFor(urlEqualTo("/rest/get?name="+myName)));
        } catch (Exception e) {
            fail("Method should no throw exception when there is no HTTP error.\n" + e.getMessage());
        }
    }
    @Test
    public void getIdBySingleQueryParamSingleValueResponseTest() {
        String accessTokenMock = "mocked access token";
        wireMockRule.stubFor(post(KeycloakHttpClient.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        try {
            String myName = "myName";
            String myId = "myId";
            String jsonBody ="{" +
                    "\"id\":\"" + myId + "\"," +
                    "\"name\":\"" + myName + "\"," +
                    "\"message\":\"this is a message body\"" +
                    "}";
            wireMockRule.stubFor(get("/rest/get").willReturn(ok().withBody(jsonBody)));
            String url = wireMockRule.baseUrl() + "/rest/get";

            assertEquals(myId,keycloakHttpClient.getIdBySingleQueryParam(null, null, url));

            verify(1,getRequestedFor(urlEqualTo("/rest/get")));
        } catch (Exception e) {
            fail("Method should no throw exception when there is no HTTP error.\n" + e.getMessage());
        }
    }

    @Test
    public void getAccessTokenTest() {
        String accessTokenMock = "mocked access token";
        wireMockRule.stubFor(post(KeycloakHttpClient.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                ));
        assertEquals(accessTokenMock, keycloakHttpClient.getAccessToken());

        verify(postRequestedFor(urlEqualTo(KeycloakHttpClient.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN))
                .withHeader("Content-Type", containing("x-www-form-urlencoded"))
                .withRequestBody(containing("username=" + System.getenv("GZL_SSO_ADMIN_USER") +
                        "&password=" + System.getenv("GZL_SSO_ADMIN_PASSWORD") + "&grant_type=password&client_id=admin-cli")));
    }

    @Test
    public void assertNoErrorStatusTest() {
        try {
            wireMockRule.stubFor(get("/good").willReturn(ok()));
            try (CloseableHttpClient httpClient = keycloakHttpClient.buildCloseableHttpClient()) {
                HttpGet goodRequest = new HttpGet(wireMockRule.baseUrl() + "/good");
                try (CloseableHttpResponse badResponse = httpClient.execute(goodRequest)) {
                    keycloakHttpClient.assertNoErrorStatus(badResponse);
                }
            }
        } catch (Exception e) {
            fail("Method should no throw exception when there is no HTTP error.\n" + e.getMessage());
        }
    }

    @Test(expected = KeycloakHttpClientException.class)
    public void assertNoErrorStatusThrowTest() throws IOException {
        try (CloseableHttpClient httpClient = keycloakHttpClient.buildCloseableHttpClient()) {
            wireMockRule.stubFor(get("/bad").willReturn(aResponse().withStatus(401)));

            HttpGet badGetRequest = new HttpGet(wireMockRule.baseUrl() + "/bad");
            try (CloseableHttpResponse badResponse = httpClient.execute(badGetRequest)) {
                keycloakHttpClient.assertNoErrorStatus(badResponse);
            }
        }
        fail("KeycloakHttpClientException should have been thrown");
    }


}