package net.ihe.gazelle.ssov7.m2m.server.interlay.factory;

import net.ihe.gazelle.ssov7.m2m.server.application.publickey.PublicKeyConfigImpl;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.KeyStorePKProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.KeycloakPKProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.LocalPKProvider;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.assertEquals;

public class PublicKeyProviderFactoryTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private PublicKeyProviderFactory publicKeyProviderFactory;

    @Test
    public void getLocalPKProvider() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "expectedKey.pem");
        publicKeyProviderFactory = new PublicKeyProviderFactory(new PublicKeyConfigImpl());
        assertEquals(LocalPKProvider.class, publicKeyProviderFactory.getPublicKeyProvider().getClass());
    }
    @Test
    public void getKeycloakPKProvider() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "https://fake-url.fake");
        publicKeyProviderFactory = new PublicKeyProviderFactory(new PublicKeyConfigImpl());
        assertEquals(KeycloakPKProvider.class, publicKeyProviderFactory.getPublicKeyProvider().getClass());
    }

    @Test
    public void getKeystorePKProvider() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "expectedKeyStore.jks");
        environmentVariables.set("JWT_VERIFY_TRUSTSTORE_KEY_ALIAS", "alias");
        environmentVariables.set("JWT_VERIFY_TRUSTSTORE_PASSWORD", "changeit");
        publicKeyProviderFactory = new PublicKeyProviderFactory(new PublicKeyConfigImpl());
        assertEquals(KeyStorePKProvider.class, publicKeyProviderFactory.getPublicKeyProvider().getClass());
    }

}