package net.ihe.gazelle.ssov7.m2m.client.application.accesstoken;

import java.util.List;

public interface AccessTokenProvider {

    /**
     * Get access token using the client credentials flow
     *
     * @param clientId     the client id
     * @param clientSecret the secret needed to authenticate to the client
     * @return the String of a JSON Web Token
     * @throws IllegalArgumentException     if clientId or clientSecret is null
     * @throws AccessTokenProviderException when unable to retrieve the access token
     */
    String getAccessToken(String clientId, String clientSecret);

    /**
     * Get access token using the client credentials flow
     *
     * @param clientId     the client id
     * @param clientSecret the secret needed to authenticate to the client
     * @param scopes       a list of scopes needed by the resource server
     * @return the String of a JSON Web Token
     * @throws IllegalArgumentException     if clientId or clientSecret is null
     * @throws AccessTokenProviderException when unable to retrieve the access token
     */
    String getAccessToken(String clientId, String clientSecret, List<String> scopes);

}
