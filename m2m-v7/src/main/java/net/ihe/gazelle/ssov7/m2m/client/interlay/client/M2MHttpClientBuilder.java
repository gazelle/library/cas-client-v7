package net.ihe.gazelle.ssov7.m2m.client.interlay.client;

import net.ihe.gazelle.ssov7.m2m.client.interlay.AccessTokenRequestInterceptor;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.List;

public class M2MHttpClientBuilder extends HttpClientBuilder {

    /**
     * Create a builder that will automatically add an access token to the request
     * @param scopes List of String of the wanted scopes
     * @return return an HttpClientBuilder with an HttpRequestInterceptor that add access token
     */
    public static HttpClientBuilder createWithAccessToken(List<String> scopes) {
        return HttpClientBuilder.create().addInterceptorFirst(new AccessTokenRequestInterceptor(scopes));
    }

    /**
     * Create a builder that will automatically add an access token to the request
     * @return return an HttpClientBuilder with an HttpRequestInterceptor that add access token
     */
    public static HttpClientBuilder createWithAccessToken() {
        return HttpClientBuilder.create().addInterceptorFirst(new AccessTokenRequestInterceptor());
    }
}
