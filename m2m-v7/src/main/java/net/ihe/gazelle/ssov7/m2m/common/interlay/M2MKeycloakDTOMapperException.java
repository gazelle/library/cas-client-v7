package net.ihe.gazelle.ssov7.m2m.common.interlay;

public class M2MKeycloakDTOMapperException extends RuntimeException {

    public M2MKeycloakDTOMapperException() {
    }

    public M2MKeycloakDTOMapperException(String s) {
        super(s);
    }

    public M2MKeycloakDTOMapperException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public M2MKeycloakDTOMapperException(Throwable throwable) {
        super(throwable);
    }
}
