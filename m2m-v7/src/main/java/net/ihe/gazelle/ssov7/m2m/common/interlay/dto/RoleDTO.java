package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;


import java.util.Objects;

public class RoleDTO {

    private String id;
    private String name;
    private boolean clientRole;
    //realm name
    private String containerId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isClientRole() {
        return clientRole;
    }

    public void setClientRole(boolean clientRole) {
        this.clientRole = clientRole;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleDTO roleDTO = (RoleDTO) o;
        return clientRole == roleDTO.clientRole
                && Objects.equals(id, roleDTO.id)
                && Objects.equals(name, roleDTO.name)
                && Objects.equals(containerId, roleDTO.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                name,
                clientRole,
                containerId
        );
    }
}
