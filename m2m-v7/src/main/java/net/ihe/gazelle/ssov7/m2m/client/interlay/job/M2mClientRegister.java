package net.ihe.gazelle.ssov7.m2m.client.interlay.job;


import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientDAO;
import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientRegistrationService;
import net.ihe.gazelle.ssov7.m2m.client.interlay.dao.M2MClientDAOImpl;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClientException;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationException;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scope(ScopeType.APPLICATION)
@AutoCreate
@Name("m2mClientRegister")
public class M2mClientRegister {
    private static final Logger LOG = LoggerFactory.getLogger(M2mClientRegister.class);
    @Observer("org.jboss.seam.postInitialization")
    public void init() {
        try {
            M2MKeycloakConfigurationServiceImpl m2MKeycloakConfigurationService = new M2MKeycloakConfigurationServiceImpl();
            String clientId = m2MKeycloakConfigurationService.getM2MClientId();
            LOG.info("Registering M2M client {} to SSO server ...", clientId);

            M2MClientDAO m2MClientDAO = new M2MClientDAOImpl(m2MKeycloakConfigurationService);
            SSOClientRegistrationService ssoClientRegistrationService = new M2MClientRegistrationService(m2MClientDAO);
            ssoClientRegistrationService.registerClient(clientId, null);
        } catch (SSOClientRegistrationException | KeycloakHttpClientException e) {
            LOG.error("Failed to register resource service to authentication server. This may leads to further issues.", e);
        }
    }
}
