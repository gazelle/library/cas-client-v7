package net.ihe.gazelle.ssov7.m2m.server.application.publickey;

import java.util.Objects;

public class PublicKeyConfigImpl implements PublicKeyConfig {

    private final String publicKeyPath;
    private final String keystoreAlias;
    private final String keystorePassword;
    private final String keyId;

    /**
     * Default constructor used when keyId is not needed.
     */
    public PublicKeyConfigImpl() {
        //using empty String to avoid NPE.
        this("");
    }

    public PublicKeyConfigImpl(String keyId) {
        publicKeyPath = Objects.requireNonNull(System.getenv("JWT_VERIFY_PUBLIC_KEY_LOCATION"),
                "Environment variable JWT_VERIFY_PUBLIC_KEY_LOCATION must not be null.");
        keystoreAlias = System.getenv("JWT_VERIFY_TRUSTSTORE_KEY_ALIAS");
        keystorePassword = System.getenv("JWT_VERIFY_TRUSTSTORE_PASSWORD");
        this.keyId = keyId;
    }

    public String getPublicKeyPath() {
        return publicKeyPath;
    }

    public String getKeystoreAlias() {
        return keystoreAlias;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public String getKeyId() {
        return keyId;
    }
}
