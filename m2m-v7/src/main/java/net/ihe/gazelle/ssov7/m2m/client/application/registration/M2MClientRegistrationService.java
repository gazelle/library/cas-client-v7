package net.ihe.gazelle.ssov7.m2m.client.application.registration;

import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationService;

public class M2MClientRegistrationService implements SSOClientRegistrationService {

    public static final String REALM = "gazelle";
    public static final String CLIENTS_URL = "/admin/realms/" + REALM + "/clients";
    public static final String MACHINE_ROLE = "machine";
    public static final String REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/master/protocol/openid-connect/token";
    public static final String ADMIN_ROLE = "role:gazelle_admin";
    private final M2MClientDAO m2MClientDAO;

    public M2MClientRegistrationService(M2MClientDAO m2MClientDAO) {
        this.m2MClientDAO = m2MClientDAO;
    }

    @Override
    public void registerClient(String clientId, String url) {
        createOrUpdateClient(clientId);
    }

    private void createOrUpdateClient(String clientId) {

        String idOfClient = m2MClientDAO.getIdOfClient(clientId);
        if (idOfClient != null && !idOfClient.isEmpty()) {
            m2MClientDAO.updateClient(idOfClient, clientId);

            String microprofileClientScopeId = m2MClientDAO.getClientScopeId("microprofile-jwt");
            m2MClientDAO.addClientScopeToClient(idOfClient,microprofileClientScopeId,false);

            String m2meClientScopeId = m2MClientDAO.getClientScopeId("m2m-client-scope");
            m2MClientDAO.addClientScopeToClient(idOfClient,m2meClientScopeId,false);
        } else {
            m2MClientDAO.createClient(clientId);
            idOfClient = m2MClientDAO.getIdOfClient(clientId);
        }
        String machineRoleId = m2MClientDAO.getRealmRoleId(MACHINE_ROLE);
        m2MClientDAO.grantRoleToClient(idOfClient, machineRoleId, MACHINE_ROLE);

        String adminRoleId = m2MClientDAO.getRealmRoleId(ADMIN_ROLE);
        m2MClientDAO.grantRoleToClient(idOfClient, adminRoleId, ADMIN_ROLE);
    }

}
