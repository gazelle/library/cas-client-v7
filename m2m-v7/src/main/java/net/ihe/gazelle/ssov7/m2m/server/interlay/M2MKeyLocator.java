package net.ihe.gazelle.ssov7.m2m.server.interlay;

import io.jsonwebtoken.LocatorAdapter;
import io.jsonwebtoken.ProtectedHeader;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.PublicKeyConfigImpl;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.factory.PublicKeyProviderFactory;

import java.security.Key;

/**
 * This class allows a {@link io.jsonwebtoken.Jwts} parser to locate automatically the public key for it to verify the
 * signature. It relies on the {@link SSOPublicKeyProvider} class to retrieve the good key.
 */
public class M2MKeyLocator extends LocatorAdapter<Key> {

    @Override
    public Key locate(ProtectedHeader header) {
        SSOPublicKeyProvider ssoPublicKeyProvider = new PublicKeyProviderFactory(
                new PublicKeyConfigImpl(header.getKeyId())).getPublicKeyProvider();
        return ssoPublicKeyProvider.getPublicKey();
    }


}
