package net.ihe.gazelle.ssov7.m2m.server.application.jwt;

public class JWTValidatorException extends RuntimeException{
    public JWTValidatorException() {
    }

    public JWTValidatorException(String s) {
        super(s);
    }

    public JWTValidatorException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public JWTValidatorException(Throwable throwable) {
        super(throwable);
    }
}
