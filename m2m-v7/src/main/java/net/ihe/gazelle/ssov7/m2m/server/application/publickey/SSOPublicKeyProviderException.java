package net.ihe.gazelle.ssov7.m2m.server.application.publickey;

public class SSOPublicKeyProviderException extends RuntimeException {

    public SSOPublicKeyProviderException() {
        super();
    }

    public SSOPublicKeyProviderException(String s) {
        super(s);
    }

    public SSOPublicKeyProviderException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SSOPublicKeyProviderException(Throwable throwable) {
        super(throwable);
    }
}
