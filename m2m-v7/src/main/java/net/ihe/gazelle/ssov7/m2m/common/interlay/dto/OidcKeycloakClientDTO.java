package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import net.ihe.gazelle.ssov7.authn.interlay.dto.KeycloakClientDTO;

import java.util.Objects;

public class OidcKeycloakClientDTO extends KeycloakClientDTO {
    private boolean authorizationServicesEnabled;
    private boolean serviceAccountsEnabled;
    private boolean publicClient;
    private boolean standardFlowEnabled;
    private boolean directAccessGrantsEnabled;
    private String secret;

    public OidcKeycloakClientDTO() {
        super("openid-connect");
    }

    public boolean isAuthorizationServicesEnabled() {
        return authorizationServicesEnabled;
    }

    public void setAuthorizationServicesEnabled(boolean authorizationServicesEnabled) {
        this.authorizationServicesEnabled = authorizationServicesEnabled;
    }

    public boolean isServiceAccountsEnabled() {
        return serviceAccountsEnabled;
    }

    public void setServiceAccountsEnabled(boolean serviceAccountsEnabled) {
        this.serviceAccountsEnabled = serviceAccountsEnabled;
    }

    public boolean isPublicClient() {
        return publicClient;
    }

    public void setPublicClient(boolean publicClient) {
        this.publicClient = publicClient;
    }

    public boolean isStandardFlowEnabled() {
        return standardFlowEnabled;
    }

    public void setStandardFlowEnabled(boolean standardFlowEnabled) {
        this.standardFlowEnabled = standardFlowEnabled;
    }

    public boolean isDirectAccessGrantsEnabled() {
        return directAccessGrantsEnabled;
    }

    public void setDirectAccessGrantsEnabled(boolean directAccessGrantsEnabled) {
        this.directAccessGrantsEnabled = directAccessGrantsEnabled;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        OidcKeycloakClientDTO that = (OidcKeycloakClientDTO) o;
        return authorizationServicesEnabled == that.authorizationServicesEnabled
                && serviceAccountsEnabled == that.serviceAccountsEnabled
                && publicClient == that.publicClient
                && standardFlowEnabled == that.standardFlowEnabled
                && directAccessGrantsEnabled == that.directAccessGrantsEnabled
                && Objects.equals(secret, that.secret);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(),
                authorizationServicesEnabled,
                serviceAccountsEnabled,
                publicClient,
                standardFlowEnabled,
                directAccessGrantsEnabled,
                secret
        );
    }
}
