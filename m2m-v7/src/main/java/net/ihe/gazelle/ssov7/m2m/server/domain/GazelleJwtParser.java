package net.ihe.gazelle.ssov7.m2m.server.domain;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Component used to parse a JWT
 */
public interface GazelleJwtParser {

    /**
     * Return when the token was issued
     * @return the Date of when the token was issued
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.6">
     *     https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.6</a>
     */
    Date getIssuedAt();

    /**
     * Return the recipient(s) of the token, to whom it is destined
     * @return a Set of all audiences found in token
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.3">
     *     https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.3</a>
     */
    Set<String> getAudience();

    /**
     * Return who issued this token
     * @return a String of the issuer
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.1">
     *     https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.1</a>
     */
    String getIssuer();

    /**
     * Return the expiration date of the token
     * @return the Date of expiration
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4>
     *     https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4</a>
     */
    Date getExpirationDate();

    /**
     * Get who is authorized by the token
     * @return a String of authorized party
     */
    String getAuthorizationParty();

    /**
     * Get the roles contained in the token
     * @return a List of roles in String format
     */
    List<String> getRoles();

    /**
     * Get scopes contained in the token
     * @return a List of scopes in String format
     */
    List<String> getScope();
}
