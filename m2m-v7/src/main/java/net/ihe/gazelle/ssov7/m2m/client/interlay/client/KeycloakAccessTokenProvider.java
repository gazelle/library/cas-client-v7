package net.ihe.gazelle.ssov7.m2m.client.interlay.client;


import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProviderException;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class KeycloakAccessTokenProvider implements AccessTokenProvider {

    public static final String REALM = "gazelle";
    public static final String REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/" + REALM + "/protocol/openid-connect/token";

    private static final Logger LOG = LoggerFactory.getLogger(KeycloakAccessTokenProvider.class);
    public static final String CLIENT_CREDENTIALS = "client_credentials";
    public static final String INVALID_SCOPE_ERROR = "invalid_scope";
    private final M2MConfigurationService m2MConfigurationService;

    private final KeycloakHttpClient keycloakHttpClient;

    public KeycloakAccessTokenProvider(M2MConfigurationService m2MConfigurationService) {
        this.m2MConfigurationService = m2MConfigurationService;
        keycloakHttpClient = new KeycloakHttpClient(m2MConfigurationService.getSSOServerBaseUrl());
    }

    @Override
    public String getAccessToken(String clientId, String clientSecret) {
        return getAccessToken(clientId, clientSecret, null, true);
    }

    @Override
    public String getAccessToken(String clientId, String clientSecret, List<String> scopes) {
        return getAccessToken(clientId, clientSecret, scopes, true);
    }

    private String getAccessToken(String clientId, String clientSecret, List<String> scopes, boolean retryIfFailed) {
        LOG.trace("getAccessToken");
        if (clientId == null || clientSecret == null)
            throw new IllegalArgumentException((clientId == null ? "clientId" : "clientSecret") + " is null");


        String keycloakUrl = m2MConfigurationService.getSSOServerBaseUrl();
        String accessToken;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost accessTokenRequest = new HttpPost(keycloakUrl + REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN);
            List<BasicNameValuePair> body = new ArrayList<>();
            body.add(new BasicNameValuePair("grant_type", CLIENT_CREDENTIALS));
            body.add(new BasicNameValuePair("client_id", clientId));
            body.add(new BasicNameValuePair("client_secret", clientSecret));
            if (scopes != null && !scopes.isEmpty())
                body.add(new BasicNameValuePair("scope", StringUtils.join(scopes, " ")));
            accessTokenRequest.setEntity(new UrlEncodedFormEntity(body));

            try (CloseableHttpResponse response = httpClient.execute(accessTokenRequest)) {
                //if it failed because of bad scope and retryIfFailed is true, then retry after adding scopes
                if (shouldRetryRequest(retryIfFailed, response)) {
                    return getAccessToken(clientId, clientSecret, scopes, false);
                }

                keycloakHttpClient.assertNoErrorStatus(response);
                HttpEntity responseEntity = response.getEntity();
                accessToken = getAccessTokenFromJson(responseEntity.getContent());
                EntityUtils.consume(responseEntity);
            }
        } catch (IOException e) {
            throw new AccessTokenProviderException("Failed to get access token", e);
        }
        LOG.trace("getAccessToken successful");
        return accessToken;
    }

    private boolean shouldRetryRequest(boolean retryIfFailed, CloseableHttpResponse response) throws IOException {
        return retryIfFailed
                && response.getStatusLine().getStatusCode() == 400
                && isInvalidScopeError(response.getEntity().getContent());
    }

    private boolean isInvalidScopeError(InputStream response) throws IOException {
        String extractedError = extractErrorFromJson(response);
        return extractedError != null && extractedError.equals(INVALID_SCOPE_ERROR);
    }

    private String extractErrorFromJson(InputStream response) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("error").getTextValue();
        } finally {
            response.close();
        }
    }

    private String getAccessTokenFromJson(InputStream response) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("access_token").getTextValue();
        } finally {
            response.close();
        }
    }

}
