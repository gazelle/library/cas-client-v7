package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;


import io.jsonwebtoken.security.Jwk;
import io.jsonwebtoken.security.Jwks;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProvider;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProviderException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.util.Iterator;

/**
 * This class is used for extraction public key from a Keycloak link.
 * It is assumed that the link returns a list of JSON Web Keys (JWK).
 *
 * @see <a href=" https://datatracker.ietf.org/doc/html/rfc7517">
 * https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.1 for more information on JWK</a>
 */
public class KeycloakPKProvider implements SSOPublicKeyProvider {

    private final String publicKeyUrl;
    private final String keyId;

    /**
     * Create a Public key provider for Keycloak link
     *
     * @param publicKeyUrl the url where the keys can be found
     * @param keyId        the id of the key used to verify the signature
     */
    public KeycloakPKProvider(String publicKeyUrl, String keyId) {
        this.publicKeyUrl = publicKeyUrl;
        this.keyId = keyId;
    }

    @Override
    public PublicKey getPublicKey() {
        if (keyId == null || keyId.isEmpty())
            throw new IllegalArgumentException("keyId is null or empty");
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet publicKeyRequest = new HttpGet(publicKeyUrl);
            try (CloseableHttpResponse response = client.execute(publicKeyRequest)) {
                assertNoErrorStatus(response);
                HttpEntity responseEntity = response.getEntity();
                PublicKey publicKey = extractPublicKeyFromJson(responseEntity.getContent(), keyId);
                EntityUtils.consume(responseEntity);
                return publicKey;
            }
        } catch (IOException e) {
            throw new SSOPublicKeyProviderException("Failed to get public key", e);
        }
    }

    private void assertNoErrorStatus(CloseableHttpResponse response) {
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST) {
            String message = response.getStatusLine().getStatusCode() + " " +
                    response.getStatusLine().getReasonPhrase();
            try {
                String body = EntityUtils.toString(response.getEntity());
                if (body != null)
                    throw new SSOPublicKeyProviderException(message + " with body: " + body);
                throw new SSOPublicKeyProviderException(message);
            } catch (IOException e) {
                throw new SSOPublicKeyProviderException(message);
            }
        }
    }

    /**
     * Extract the public key with the good id.
     *
     * @param jsonKeys a JSON containing a list of JWKs
     * @param keyId    the id of the key to return
     * @return return the public key if found
     * @throws SSOPublicKeyProviderException if an error occurs or if the key is not found.
     */
    private PublicKey extractPublicKeyFromJson(InputStream jsonKeys, String keyId) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonKeys).get("keys");
            if (jsonNode.isArray()) {
                Iterator<JsonNode> jsonNodeIterator = jsonNode.getElements();
                while (jsonNodeIterator.hasNext()) {
                    JsonNode currentNode = jsonNodeIterator.next();
                    if (currentNode.get("kid").getTextValue().equals(keyId)) {
                        Jwk<?> jwk = Jwks.parser().build().parse(currentNode.toString());
                        return (PublicKey) jwk.toKey();
                    }
                }
            }
        } catch (Exception e) {
            throw new SSOPublicKeyProviderException("Error while extracting public key", e);
        }
        throw new SSOPublicKeyProviderException(String.format("No public key found with key id : %s", keyId));
    }
}
