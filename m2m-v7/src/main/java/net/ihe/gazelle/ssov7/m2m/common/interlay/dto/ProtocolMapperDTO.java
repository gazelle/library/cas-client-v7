package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ProtocolMapperDTO {

    private String id;
    private String name;
    private String protocol;
    private String protocolMapper;
    private Map<String, String> config = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocolMapper() {
        return protocolMapper;
    }

    public void setProtocolMapper(String protocolMapper) {
        this.protocolMapper = protocolMapper;
    }

    public Map<String, String> getConfig() {
        return new HashMap<>(config);
    }

    public void setConfig(Map<String, String> config) {
        this.config = config != null ?
                new HashMap<>(config) :
                new HashMap<String, String>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProtocolMapperDTO that = (ProtocolMapperDTO) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(protocol, that.protocol)
                && Objects.equals(protocolMapper, that.protocolMapper)
                && Objects.equals(config, that.config);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                name,
                protocol,
                protocolMapper,
                config
        );
    }
}
