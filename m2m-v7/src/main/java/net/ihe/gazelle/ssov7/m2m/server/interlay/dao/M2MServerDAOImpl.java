package net.ihe.gazelle.ssov7.m2m.server.interlay.dao;


import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakDTOMapper;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient;
import net.ihe.gazelle.ssov7.m2m.server.application.registration.M2MServerDAO;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.ADMIN_REALMS_GAZELLE;
import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.CLIENT_SCOPES;


@Scope(ScopeType.APPLICATION)
@AutoCreate
@Name("m2MServerDAOImpl")
public class M2MServerDAOImpl implements M2MServerDAO {
    public static final String PROTOCOL_MAPPERS_MODELS = "/protocol-mappers/models";

    @In(value = "m2mKeycloakConfigurationServiceImpl")
    private M2MConfigurationService m2MConfigurationService;

    private KeycloakHttpClient keycloakHttpClient;

    private M2MKeycloakDTOMapper keycloakDTOMapper;
    private String keycloakUrl;

    public M2MServerDAOImpl() {
        //mandatory empty constructor for Injection
    }

    public M2MServerDAOImpl(M2MConfigurationService m2MConfigurationService) {
        this.m2MConfigurationService=m2MConfigurationService;
        this.keycloakUrl = this.m2MConfigurationService.getSSOServerBaseUrl();
        keycloakDTOMapper = new M2MKeycloakDTOMapper();
        keycloakHttpClient = new KeycloakHttpClient(this.keycloakUrl);
    }

    @Override
    public String getClientScopeId(String clientScopeName) {
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES;
        return keycloakHttpClient.getIdBySingleQueryParam("name", clientScopeName, clientScopeUrl);
    }


    @Override
    public boolean scopeMappingOfRoleExists(String clientScopeId, String roleId) {
        String scopeMappingUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + "/scope-mappings/realm";
        String id = keycloakHttpClient.getIdBySingleQueryParam("id", roleId, scopeMappingUrl);
        return id != null && !id.isEmpty();
    }

    @Override
    public void createScopeMappingOfRole(String clientScopeId, String roleId, String roleName) {
        String scopeMappingUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId + "/scope-mappings/realm";
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(scopeMappingUrl),
                "[" + keycloakDTOMapper.createRealmRoleRepresentationJson(roleId, roleName) + "]");
    }


    @Override
    public void createProtocolMapperAudience(String clientScopeId, String audience) {
        String protocolMapperUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(protocolMapperUrl),
                keycloakDTOMapper.createProtocolMapperAudienceRepresentationJson(audience)
        );
    }

    @Override
    public void updateProtocolMapperAudience(String protocolMapperAudienceId, String clientScopeId, String audience) {
        String protocolMapperUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS + "/" + protocolMapperAudienceId;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPut(protocolMapperUrl),
                keycloakDTOMapper.createProtocolMapperAudienceRepresentationJson(audience, protocolMapperAudienceId)
        );
    }

    @Override
    public void createProtocolMapperAuthzMethod(String clientScopeId) {
        String protocolMapperUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(protocolMapperUrl),
                keycloakDTOMapper.createProtocolMapperAuthzMethodRepresentationJson()
        );
    }

    @Override
    public void updateProtocolMapperAuthzMethod(String protocolMapperAudienceId, String clientScopeId) {
        String protocolMapperUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS + "/" + protocolMapperAudienceId;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPut(protocolMapperUrl),
                keycloakDTOMapper.createProtocolMapperAuthzMethodRepresentationJson(protocolMapperAudienceId)
        );
    }

    @Override
    public String getProtocolMapperId(String clientScopeId, String mapperName) {
        String protocolMapperUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId
                + PROTOCOL_MAPPERS_MODELS;
        return keycloakHttpClient.getIdBySingleQueryParam("name", mapperName, protocolMapperUrl);
    }

    @Override
    public void createClientScope(String clientScopeName, String clientScopeDescription) {
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(clientScopeUrl),
                keycloakDTOMapper.createM2mClientScopeRepresentationJson(clientScopeName, clientScopeDescription));
    }

    @Override
    public String getRealmRoleId(String roleName) {
        String roleUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/roles";
        return keycloakHttpClient.getIdBySingleQueryParam("name", roleName, roleUrl);
    }


    @Override
    public void updateClientScope(String clientScopeId, String clientScopeName, String createClientScope) {
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES + "/" + clientScopeId;
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPut(clientScopeUrl),
                keycloakDTOMapper.createM2mClientScopeRepresentationJson(clientScopeName, createClientScope));
    }

}


