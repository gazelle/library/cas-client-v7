package net.ihe.gazelle.ssov7.m2m.common.interlay.client;

import net.ihe.gazelle.ssov7.authn.interlay.client.ServerErrorRetryHandler;
import net.ihe.gazelle.ssov7.authn.interlay.client.ServerErrorRetryStrategy;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Iterator;

public class KeycloakHttpClient {
    public static final String BEARER = "bearer ";
    public static final String REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/master/protocol/openid-connect/token";
    public static final String ADMIN_REALMS_GAZELLE = "/admin/realms/gazelle";
    public static final String CLIENT_SCOPES = "/client-scopes";
    private String accessToken;
    private static final Logger LOG = LoggerFactory.getLogger(KeycloakHttpClient.class);
    private final String keycloakUrl;
    private final int retryInterval;
    private final int retryCount;

    /**
     * Constructor for KeycloakHttpClient with by default a count of 3 retries
     * and 5 seconds interval between each retry
     *
     * @param keycloakUrl : the base url of your Keycloak
     */
    public KeycloakHttpClient(String keycloakUrl) {
        this(keycloakUrl, 3, 5000);
    }

    /**
     * Constructor for KeycloakHttpClient
     *
     * @param keycloakUrl   : the base url of your Keycloak
     * @param retryCount    : the number of times a request should be retried in case of error
     * @param retryInterval : the time between each retry (in milliseconds)
     */
    public KeycloakHttpClient(String keycloakUrl, int retryCount, int retryInterval) {
        this.keycloakUrl = keycloakUrl;
        this.retryInterval = retryInterval;
        this.retryCount = retryCount;
    }

    public void executeRequestWithJsonBody(HttpEntityEnclosingRequestBase resourceRequest, String resourceJson) {
        LOG.trace("begin executeRequest");

        try (CloseableHttpClient httpClient = buildCloseableHttpClient()) {
            resourceRequest.addHeader(HttpHeaders.AUTHORIZATION, BEARER + getAccessToken());
            resourceRequest.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
            resourceRequest.setEntity(new StringEntity(resourceJson));

            try (CloseableHttpResponse response = httpClient.execute(resourceRequest)) {
                assertNoErrorStatus(response);
                EntityUtils.consume(response.getEntity());
            }

        } catch (IOException e) {
            throw new KeycloakHttpClientException("executeRequest failed", e);
        }
        LOG.trace("executeRequest successful");
    }

    /**
     * Retrieve id from a GET Request
     *
     * @param paramName  : a String of the query parameter name
     * @param paramValue : a String of the query parameter value
     * @param url        : a String of the url endpoint
     * @return a String of the attribute
     */
    public String getIdBySingleQueryParam(String paramName, String paramValue, String url) {
        HttpGet getRequest = new HttpGet();
        try (CloseableHttpClient httpClient = buildCloseableHttpClient()) {
            if (paramName != null && !paramName.isEmpty()) {
                URI uri = new URIBuilder(url).setParameter(paramName, paramValue).build();
                getRequest.setURI(uri);
            } else {
                getRequest.setURI(new URIBuilder(url).build());
            }

            getRequest.addHeader(HttpHeaders.AUTHORIZATION, BEARER + getAccessToken());

            try (CloseableHttpResponse response = httpClient.execute(getRequest)) {
                assertNoErrorStatus(response);
                HttpEntity responseEntity = response.getEntity();
                String id;
                if ((paramName == null) || paramName.isEmpty()) {
                    id = getIdInJson(responseEntity.getContent());
                } else {
                    id = getIdInJsonArray(paramName, paramValue, responseEntity.getContent());
                }
                EntityUtils.consume(responseEntity);
                LOG.trace("getIdByAttribute successful");
                return id;
            }
        } catch (IOException | URISyntaxException e) {
            throw new KeycloakHttpClientException(
                    String.format("getIdByAttribute failed with attribute %s = %s", paramName, paramValue),
                    e
            );
        }
    }

    /**
     * Get access token with the privileges of the user defined by GZL_SSO_ADMIN_USER variable
     *
     * @return a String of the encoded access token
     */
    public String getAccessToken() {
        LOG.trace("enter getAccessToken");
        if (accessToken == null) {
            try (CloseableHttpClient httpClient = buildCloseableHttpClient()) {
                HttpPost accessTokenRequest = new HttpPost(keycloakUrl + REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN);
                accessTokenRequest.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
                accessTokenRequest.setEntity(new UrlEncodedFormEntity(Arrays.asList(

                        new BasicNameValuePair("username", System.getenv("GZL_SSO_ADMIN_USER")),
                        new BasicNameValuePair("password", System.getenv("GZL_SSO_ADMIN_PASSWORD")),
                        new BasicNameValuePair("grant_type", "password"),
                        new BasicNameValuePair("client_id", "admin-cli")
                )));
                try (CloseableHttpResponse response = httpClient.execute(accessTokenRequest)) {
                    assertNoErrorStatus(response);
                    HttpEntity responseEntity = response.getEntity();
                    this.accessToken = getAccessTokenFromJson(responseEntity.getContent());
                    EntityUtils.consume(responseEntity);
                }
            } catch (IOException e) {
                throw new KeycloakHttpClientException("getAccessToken failed", e);
            }
        }
        LOG.trace("getAccessToken successful");
        return accessToken;
    }

    private String getAccessTokenFromJson(InputStream response) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("access_token").getTextValue();
        } finally {
            response.close();
        }
    }

    /**
     * Assert that the response did not return an error
     *
     * @throws KeycloakHttpClientException if the response http status code is 400 or more
     */
    public void assertNoErrorStatus(CloseableHttpResponse response) {
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST) {
            String message = response.getStatusLine().getStatusCode() + " " +
                    response.getStatusLine().getReasonPhrase();
            try {
                String body = EntityUtils.toString(response.getEntity());
                if (body != null)
                    throw new KeycloakHttpClientException(message + " with body: " + body);
                throw new KeycloakHttpClientException(message);
            } catch (IOException e) {
                throw new KeycloakHttpClientException(message);
            }
        }
    }

    /**
     * Return a custom CloseableHttpClient with a custom number of retries and custom retry interval
     *
     * @return a CloseableHttpClient
     */
    public CloseableHttpClient buildCloseableHttpClient() {
        return HttpClients.custom()
                .setServiceUnavailableRetryStrategy(new ServerErrorRetryStrategy(retryInterval, retryCount))
                .setRetryHandler(new ServerErrorRetryHandler(retryCount))
                .build();
    }


    private String getIdInJson(InputStream response) throws IOException {
        try {
            JsonNode jsonNode = new ObjectMapper().readTree(response);
            return jsonNode.get("id").getTextValue();
        } finally {
            response.close();
        }

    }

    /**
     * Retrieve an attribute from a JSON
     *
     * @param paramName  : a String of the parameter name to match in JSON
     * @param paramValue : a String of the parameter value to match in JSON
     * @param response   : an InputStream of the response from a GET request
     * @return a String of the attribute
     * @throws IOException if there are failed or interrupted I/O operations
     */
    private String getIdInJsonArray(String paramName, String paramValue, InputStream response) throws IOException {
        try {
            JsonNode jsonNode = new ObjectMapper().readTree(response);
            if (jsonNode.isArray()) {
                Iterator<JsonNode> jsonNodeIterator = jsonNode.getElements();
                while (jsonNodeIterator.hasNext()) {
                    JsonNode currentNode = jsonNodeIterator.next();
                    if (currentNode.get(paramName).getTextValue().equals(paramValue))
                        return currentNode.get("id").getTextValue();
                }
            }
            return null;
        } finally {
            response.close();
        }
    }
}
