package net.ihe.gazelle.ssov7.m2m.client.application.accesstoken;

public class AccessTokenProviderException extends RuntimeException {

    public AccessTokenProviderException() {
    }

    public AccessTokenProviderException(String s) {
        super(s);
    }

    public AccessTokenProviderException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AccessTokenProviderException(Throwable throwable) {
        super(throwable);
    }
}
