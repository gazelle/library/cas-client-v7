package net.ihe.gazelle.ssov7.m2m.server.interlay;

import java.io.Serializable;
import java.security.Principal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class M2MPrincipal implements Principal, Serializable {

    private static final long serialVersionUID = -4230774259425197488L;
    private final String jwt;
    private Set<String> scopes = new HashSet<>();

    public M2MPrincipal(String jwt) {
        this.jwt = jwt;
    }

    @Override
    public String getName() {
        return this.jwt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        M2MPrincipal that = (M2MPrincipal) o;
        return Objects.equals(jwt, that.jwt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jwt);
    }

    public Set<String> getScopes() {
        return new HashSet<>(scopes);
    }

    public void setScopes(Set<String> scopes) {
        this.scopes = scopes != null
                ? new HashSet<>(scopes)
                : new HashSet<String>();
    }

    public String getJwt() {
        return jwt;
    }
}
