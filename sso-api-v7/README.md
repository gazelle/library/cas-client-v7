# Sso-api-v7

<!-- TOC -->
* [Sso-api-v7](#sso-api-v7)
  * [Authentication (authn)](#authentication-authn)
    * [Identity](#identity)
    * [Authenticator](#authenticator)
  * [Configuration](#configuration)
  * [Registration](#registration)
<!-- TOC -->

This module contains Java interfaces for user identity, configuration and registration.

## Authentication (authn)

In this package you can find interfaces related to authentication.

### Identity

You will find the GazelleIdentity component. It is used to get the identity of a user interacting with the application. 
Inside you will find methods to retrieve the user attributes.

### Authenticator

Sometimes an application can allow multiple types of authentication at the same time (like CAS and M2M). If you want to 
populate a GazelleIdentity you may add support for every authentication methods in GazelleIdentityImpl result in a bloated 
class that is difficult to maintain. Plus, it means that you support every method even if they are not enabled. Another
solution would be to implement a new GazelleIdentity but when need to inject it you need to know in advance witch type of 
authentication is used.

Authenticators helps you with these problems. One Authenticator represent one type of authentication. The default implementation
when injecting GazelleIdentity is GazelleIdentityImpl They are called in
GazelleIdentityImpl by a factory that select the Authenticator corresponding to the current authentication method. They
use SPI which means that you can add new Authenticators and still be able to inject GazelleIdentity without any modification
in the implementation.

You can find real example [here](../cas-client-v7/src/main/java/net/ihe/gazelle/ssov7/authn/interlay/authenticators).

The authenticator factories are used to create an Authenticator. They are also used to determine if an authentication 
method is enabled which is required to get the correct Authenticator.

You can find real example [here](../cas-client-v7/src/main/java/net/ihe/gazelle/ssov7/authn/interlay/factory)
(CasAuthenticatorFactory and IpAuthenticatorFactory).

The Authenticators were created with GazelleIdentityImpl which is why they return an AuthenticatorResource. This resource
is used to store all the attribute used to populate a GazelleIdentity.

## Configuration

In the **configuration** package, you will find an Interface used to get the base URL of the SSO server. 

## Registration

In the **registration** package, you will find an Interface used to register the application as client to the SSO server
(ex: Keycloak).
