package net.ihe.gazelle.ssov7.configuration.domain;

public interface SSOConfigurationService {

    /**
     * Retrieve the base url to the SSO server
     *
     * @return the base url to the SSO server
     * @throws SSOConfigurationServiceException if the url cannot be retrieved
     */
    String getSSOServerBaseUrl();
}
