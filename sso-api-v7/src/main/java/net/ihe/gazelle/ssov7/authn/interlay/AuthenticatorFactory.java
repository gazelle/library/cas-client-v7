package net.ihe.gazelle.ssov7.authn.interlay;

/**
 * Component used for Authenticator creation
 * @see Authenticator
 */
public interface AuthenticatorFactory {

    /**
     * Create a new Authenticator
     * @return an Authenticator
     */
    Authenticator createAuthenticator();

    /**
     * Is this factory enabled
     * @return true if the authentication method is enabled, false otherwise
     */
    boolean isEnabled();
}
