# Gazelle SSO Client v7

This library is used by Gazelle web applications to authenticate users with an SSO server or by IP
Login. For JDK-7 stack.

<!-- TOC -->
* [Gazelle SSO Client v7](#gazelle-sso-client-v7)
  * [Introduction](#introduction)
  * [Build and tests](#build-and-tests)
  * [Setup](#setup)
  * [Migration guide](#migration-guide)
<!-- TOC -->

## Introduction

The current implementation is based on CAS protocol 3.0 and has been tested on __Apereo CAS__ and
__Keycloak__ with CAS plugin extension.

The project contains 4 modules:

* [__sso-api-v7__](sso-api-v7/README.md) that contains Java interfaces to get user identity, to access configuration and
  to register the application as a client of the SSO server.
* [__sso-commons-v7__](sso-commons-v7/README.md) provides elements used by this project modules
* [__cas-client-v7__](cas-client-v7/README.md) contains the client implementations of __sso-api-v7__ for CAS Protocol based on
  JBoss-7.2.0.Final and JDK-7 stack. It also contains different filters to be added in the web.xml by the application.
* [__cas-client-ui-v7__](cas-client-ui-v7/README.md) contains JSF components to have the login button and drop down menu.
* [__m2m-v7__](m2m-v7/README.md) contains necessary component to enable m2m in your project. It contains a http client 
  builder to add m2m to your clients in Java 7. It contains an interface for access token provider with an implementation 
  for Keycloak. It also contains public key providers and JWT validator to add m2m to your server.

## Build and tests

The project must be built using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Setup

To use sso-client-v7 components, you need to add the following dependency in the root pom of your project:

```xml
<dependencyManagement>
  <dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>sso-client-v7</artifactId>
    <version>${sso.client.v7.version}</version>
    <type>pom</type>
    <scope>import</scope>
  </dependency>
</dependencyManagement>
```

Then add the dependency of the module(s) you need:
* For sso-api-v7:
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>sso-api-v7</artifactId>
</dependency>
```
* For cas-client-v7:
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>cas-client-v7</artifactId>
</dependency>
```
* For cas-client-ui-v7:
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>cas-client-ui-v7</artifactId>
  <type>war</type>
</dependency>
```
* For sso-commons-v7:
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>sso-commons-v7</artifactId>
</dependency>
```
* For gum-client: 
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>gum-ws-client</artifactId>
</dependency>
```
* For m2m-v7:
```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>m2m-v7</artifactId>
</dependency>
```

## Migration guide

You can find a migration guide [here](migration-guide.md).
