# Cas-client-v7

<!-- TOC -->
* [Cas-client-v7](#cas-client-v7)
  * [Single SSO server usage](#single-sso-server-usage)
  * [Filters](#filters)
    * [AuthenticationFilter](#authenticationfilter)
    * [IpAuthenticationFilter](#ipauthenticationfilter)
    * [LogoutFilter](#logoutfilter)
    * [SessionExpirationFilter](#sessionexpirationfilter)
      * [Session timeout](#session-timeout)
<!-- TOC -->

This module contains the implementation of the sso-client-v7 interfaces. It is a client for the Keycloak CAS protocol
usable by Gazelle applications.
There are also some filters that can be used by the applications.

>:warning: **Warning:**
> 
> For CAS authentication to work, this module must be paired with the cas-client-ui-v7 module.
>
> [See here](../cas-client-ui-v7/README.md) for more information.

## Single SSO server usage

1. Add as dependencies in your ejb
   ```xml
   <dependency>
      <groupId>net.ihe.gazelle</groupId>
      <artifactId>sso-api-v7</artifactId>
      <version>${sso.client.v7.version}</version>
   </dependency>
   <dependency>
      <groupId>net.ihe.gazelle</groupId>
      <artifactId>cas-client-v7</artifactId>
      <version>${sso.client.v7.version}</version>
   </dependency>
   ```
   
2. Update your web.xml like this:

    ```xml
    <context-param>
        <param-name>configurationStrategy</param-name>
        <param-value>PROPERTY_FILE</param-value>
    </context-param>

    <context-param>
        <param-name>configFileLocation</param-name>
        <param-value>/opt/gazelle/cas/${finalName}.properties</param-value>
    </context-param>
   
    <filter>
        <filter-name>CookieBrowserFilter</filter-name>
        <filter-class>net.ihe.gazelle.common.servletfilter.CookieBrowserFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CookieBrowserFilter</filter-name>
        <url-pattern>*.seam</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <listener>
        <listener-class>org.jasig.cas.client.session.SingleSignOutHttpSessionListener</listener-class>
    </listener>

    <filter>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <url-pattern>/login/login.seam</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.LogoutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <url-pattern>/cas/logout.seam</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Validation Filter</filter-name>
        <filter-class>org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter
        </filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Validation Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
   
    <filter>
        <filter-name>ExpirationFilter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.SessionExpirationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>ExpirationFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    ```
   
3. CAS Configuration file

To configuration SSO accesses, you must create the following
file : `/opt/gazelle/cas/${finalName}.properties` according to the `configFileLocation` param defined
in your `web.xml`.

Example for gazelle-tm, the file to create is `/opt/gazelle/cas/gazelle-tm.properties`.

Four properties must be set in this file :

- service -> specify the service URL of your application
- casServerUrlPrefix -> The prefix url for the CAS server
- casServerLoginUrl -> The login url for the CAS server
- casLogoutUrl -> The logout url for the CAS server

Here an example of configuration to use with __Gazelle-User-Management__ (Keycloak with CAS plugin):

```properties
service=http://example.com/app/
casServerUrlPrefix=https://example.com/auth/realms/gazelle/protocol/cas
casServerLoginUrl=https://example.com/auth/realms/gazelle/protocol/cas/login
casLogoutUrl=https://example.com/auth/realms/gazelle/protocol/cas/logout
```

4. Required preferences

Your application must provide an SPI implementation of `PreferenceService` from
gazelle-preferences (preferences-v7)

   ```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>gazelle-preferences</artifactId>
    <version>1.1.0</version>
</dependency>
   ```

   ```java

@MetaInfServices(PreferenceProvider.class)
public class MyAppPreferenceProvider implements PreferenceProvider {
    //...
}
   ```

The following preferences must be provided at runtime:

* `cas_enabled`: Enable/disable CAS login integration.
* `ip_login`: Enable/disable login via IP filtering mechanism (do not use in
  production). `ip_login_admin` regexp must be defined if set to true.
* `ip_login_admin`: Regular expression to filter IP addresses for which users will be identified
  as admin users. `ip_login` must be set to true to be effective.

5. Client registration

* To use the client registration, you need to add the Metadata v7 dependency:

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>metadata-v7</artifactId>
    <version>${metadata.v7.version}</version>
    <type>ejb</type>
</dependency>
```

And follow the [Metadata v7 README](https://gitlab.inria.fr/gazelle/public/framework/metadata-v7) for properties to
add.

* To use the cas-client library with Keycloak, add the following environment variable

| Name                            | Description                                                                                     | Value Example |
|---------------------------------|-------------------------------------------------------------------------------------------------|---------------|
| GZL_SSO_ADMIN_USER              | Name of an admin that has manage-clients role in Keycloak master realm                          | username      |
| GZL_SSO_ADMIN_PASSWORD          | Password of an admin that has manage-clients role in Keycloak master realm                      | password      |
| CAS_REGISTRATION_RETRY_INTERVAL | Interval between sent request when there is a server error, default value is 30000 (30 seconds) | 5000          |

## Filters

### AuthenticationFilter

The filter named __AuthenticationFilter__ is used to redirect the user to the Keycloak server when it
browses `/login/login.seam` url.

This filter also manage all redirections regarding authentication process (after login, after timeout, ...).

The filter can be added in the web.xml of the application with this following code :

```xml
<filter>
    <filter-name>Gazelle CAS Authentication Filter</filter-name>
    <filter-class>net.ihe.gazelle.cas.client.authentication.AuthenticationFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>Gazelle CAS Authentication Filter</filter-name>
    <url-pattern>/login/login.seam</url-pattern>
</filter-mapping>
```


### IpAuthenticationFilter

The filter named __IpAuthenticationFilter__ is used to log the user when ip login is enabled when it
browses `/login/login.seam` url.

This filter also manage all redirections regarding authentication process.

The filter can be added in the web.xml of the application with this following code :

````xml
<filter>
    <filter-name>Gazelle Ip Authentication Filter</filter-name>
    <filter-class>net.ihe.gazelle.ssov7.authn.interlay.filter.IpAuthenticationFilter</filter-class>
    </filter>
<filter-mapping>
    <filter-name>Gazelle Ip Authentication Filter</filter-name>
    <url-pattern>/login/ipLogin.seam</url-pattern>
</filter-mapping>
````


### LogoutFilter

The filter named __LogoutFilter__ is used to redirect the user to the Keycloak server when it browses `/cas/logout.seam`
url.

The filter can be added in the web.xml of the application with this following code :

```xml
<filter>
    <filter-name>Gazelle CAS logout filter</filter-name>
    <filter-class>net.ihe.gazelle.cas.client.authentication.LogoutFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>Gazelle CAS logout filter</filter-name>
    <url-pattern>/cas/logout.seam</url-pattern>
</filter-mapping>
```

### SessionExpirationFilter

The filter named __SessionExpirationFilter__ is used to manage the expiration of the sessions.

This filter will be call each time a user perform an action on the application.

It will check that the session is still valid. If not, it will invalidate the session. This invalidation will force a
renewal of the session by the __AuthenticationFilter__.

If the session is still active on Server side, the user will be re-authenticated without any action from him. If not, he
will be redirected to the login page.

The filter can be added in the web.xml of the application with this following code :

```xml
<filter>
    <filter-name>ExpirationFilter</filter-name>
    <filter-class>net.ihe.gazelle.cas.client.authentication.SessionExpirationFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>ExpirationFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
```

#### Session timeout

The timeout of the session must be configured in the web.xml of each application. By default, set this value to 20
minutes.

This configuration will delete the user session on the application after 20 minutes of inactivity.

Example of code to configure the session timeout :

```xml
<session-config>
    <session-timeout>20</session-timeout>
</session-config>
```
