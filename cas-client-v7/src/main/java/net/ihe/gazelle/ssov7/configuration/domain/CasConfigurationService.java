package net.ihe.gazelle.ssov7.configuration.domain;

public interface CasConfigurationService extends SSOConfigurationService {
    /**
     * Retrieve the url to reset the password on the SSO server
     * @return the url to reset the password on the SSO server
     * @throws SSOConfigurationServiceException if the url cannot be retrieved
     */
    String getSSOResetPasswordUrl();
    /**
     * Retrieve the client id of the SSO CAS client
     * @return the client id of the CAS client
     */
    String getCASClientId();
}
