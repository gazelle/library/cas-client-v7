package net.ihe.gazelle.ssov7.authn.interlay.authenticators;

import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import net.ihe.gazelle.ssov7.authn.interlay.helper.AuthenticatorHelper;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.jboss.seam.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class CasAuthenticator implements Authenticator {

    private static final Logger LOG = LoggerFactory.getLogger(CasAuthenticator.class);

    private static final String INSTITUTION_KEYWORD_TICKET_ATTRIBUTE = "institution_keyword";
    private static final String FIRSTNAME_TICKET_ATTRIBUTE = "firstName";
    private static final String LASTNAME_TICKET_ATTRIBUTE = "lastName";
    private static final String EMAIL_TICKET_ATTRIBUTE = "email";
    private static final String ROLES_TICKET_ATTRIBUTE = "role_name";
    private final AuthenticatorHelper authenticatorHelper;

    public CasAuthenticator() {
        authenticatorHelper = new AuthenticatorHelper();
    }

    @Override
    public AuthenticatorResource authenticate() {
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        AttributePrincipal casPrincipal = getPrincipal();
        if (casPrincipal != null && casPrincipal.getName() != null) {
            authenticatorResource.setPrincipal(casPrincipal);
            authenticatorResource.setUsername(casPrincipal.getName());

            // Update attributes for user
            final Map<String, Object> attributes = casPrincipal.getAttributes();
            authenticatorResource.setOrganizationKeyword((String) attributes.get(INSTITUTION_KEYWORD_TICKET_ATTRIBUTE));
            authenticatorResource.setRoles((String) attributes.get(ROLES_TICKET_ATTRIBUTE));
            authenticatorResource.setFirstName((String) attributes.get(FIRSTNAME_TICKET_ATTRIBUTE));
            authenticatorResource.setLastName((String) attributes.get(LASTNAME_TICKET_ATTRIBUTE));
            authenticatorResource.setEmail((String) attributes.get(EMAIL_TICKET_ATTRIBUTE));


            LOG.info("{} has logged in.", authenticatorResource.getUsername());
            for (final Map.Entry<String, Object> object : attributes.entrySet()) {
                LOG.debug("       {}={}", object.getKey(), object.getValue());
            }
            authenticatorResource.setLoggedIn(true);
            return authenticatorResource;
        }
        authenticatorResource.setLoggedIn(false);
        return authenticatorResource;
    }

    @Override
    public boolean isReadyToLogin() {
        AttributePrincipal casPrincipal = getPrincipal();
        return casPrincipal != null && casPrincipal.getName() != null;
    }

    @Override
    public String getLogoutType() {
        return "casLogout";
    }

    @Override
    public void logout() {
        final HttpServletRequest request = authenticatorHelper.getHttpServletRequestFromFaceContext();
        if (request != null) {
            request.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
            final HttpSession session = request.getSession(false);
            session.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
            Session.instance().invalidate();
        }
    }

    public AttributePrincipal getPrincipal() {
        final HttpServletRequest request = authenticatorHelper.getHttpServletRequestFromFaceContext();
        if (request != null) {
            final HttpSession session = request.getSession(false);
            final Assertion assertion = (Assertion) (session != null ?
                    session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) :
                    request.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
            return assertion != null && assertion.isValid() ? assertion.getPrincipal() : null;
        }
        return null;
    }
}
