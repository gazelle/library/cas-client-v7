package net.ihe.gazelle.ssov7.authn.interlay.factory;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorFactory;
import net.ihe.gazelle.ssov7.authn.interlay.authenticators.CasAuthenticator;

public class CasAuthenticatorFactory implements AuthenticatorFactory {
    private final PreferenceProvider preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
    @Override
    public Authenticator createAuthenticator() {
        return new CasAuthenticator();
    }

    @Override
    public boolean isEnabled() {
        return preferenceProvider.getBoolean("cas_enabled");
    }
}
