package net.ihe.gazelle.ssov7.configuration.interlay;

public class GumConfigClientException extends RuntimeException{

    public GumConfigClientException() {
        super();
    }

    public GumConfigClientException(String message) {
        super(message);
    }

    public GumConfigClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public GumConfigClientException(Throwable cause) {
        super(cause);
    }
}
