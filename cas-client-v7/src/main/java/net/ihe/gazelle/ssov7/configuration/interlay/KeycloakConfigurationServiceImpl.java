package net.ihe.gazelle.ssov7.configuration.interlay;

import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.interlay.MetadataServiceProviderImpl;
import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.configuration.domain.CasConfigurationService;
import net.ihe.gazelle.ssov7.configuration.domain.SSOConfigurationServiceException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.ServletLifecycle;
import org.slf4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

@Scope(ScopeType.APPLICATION)
@AutoCreate
@Name("keycloakConfigurationService")
public class KeycloakConfigurationServiceImpl implements CasConfigurationService {

    Logger logger = org.slf4j.LoggerFactory.getLogger(KeycloakConfigurationServiceImpl.class);
    private final String casPropertiesFile;
    private final PreferenceProvider preferenceProvider;

    private final MetadataServiceProvider metadataServiceProvider;

    public KeycloakConfigurationServiceImpl() {
        // Retrieve context-param from web.xml for configFileLocation
        metadataServiceProvider = new MetadataServiceProviderImpl();
        casPropertiesFile = ServletLifecycle.getServletContext().getInitParameter("configFileLocation");
        preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
    }

    @Override
    public String getSSOResetPasswordUrl() {
        assertCasEnabled();
        try {
            // Read casServerUrlPrefix property in cas.properties file
            String getCasResetPasswordUrl = getCasServerLoginUrl(casPropertiesFile).replaceAll("/realms/(.*)/protocol.*", "/realms/$1/login-actions/reset-credentials");

            // Append client_id to reset password url for redirection
            getCasResetPasswordUrl = getCasResetPasswordUrl + "?client_id=" + this.getCASClientId();
            logger.debug("getCasResetPasswordUrl: {}", getCasResetPasswordUrl);
            return getCasResetPasswordUrl;
        } catch (FileNotFoundException e) {
            throw new SSOConfigurationServiceException(e.getMessage());
        }
    }

    public String getCASClientId() {
        // Return clientId
        return "cas-" + getClientId();
    }


    @Override
    public String getSSOServerBaseUrl() {
        try {
            String getCasServerApiUrl = getCasServerLoginUrl(casPropertiesFile).replaceAll("/realms.*", "");
            logger.debug("getCasServerBaseUrl: {}", getCasServerApiUrl);
            return getCasServerApiUrl;
        } catch (FileNotFoundException e) {
            throw new SSOConfigurationServiceException(e.getMessage());
        }
    }


    private String getClientId() {
        Service service = metadataServiceProvider.getMetadata();
        // Retrieve instanceId
        String instanceId = service.getInstanceId();
        // Retrieve serviceName
        String serviceName = service.getName();
        return serviceName + (instanceId == null ? "" : "-" + instanceId);
    }

    private void assertCasEnabled() {
        if (Boolean.FALSE.equals(preferenceProvider.getBoolean("cas_enabled")))
            throw new SSOConfigurationServiceException("CAS is not enabled, this method must not be called");
    }

    private String getCasServerLoginUrl(String filename) throws FileNotFoundException {
        Properties props = new Properties();
        try (InputStream inputStream = Files.newInputStream(Paths.get(filename))) {
            props.load(inputStream);
            return props.get("casServerLoginUrl").toString();
        } catch (IOException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }
}
