package net.ihe.gazelle.cas.client.authentication;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jasig.cas.client.Protocol;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class LogoutFilter extends AbstractCasFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LogoutFilter.class);
    public static final String APPLICATION_URL = "application_url";

    private String casServerLogoutUrl;
    private String applicationUrl;

    public LogoutFilter() {
        super(Protocol.CAS2);
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getCasServerLogoutUrl() {
        return casServerLogoutUrl;
    }

    public void setCasServerLogoutUrl(String casServerLogoutUrl) {
        this.casServerLogoutUrl = casServerLogoutUrl;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException,
            ServletException {
        StringBuilder logoutUrlBuilder = new StringBuilder();
        try {
            logoutUrlBuilder.append(getCasServerLogoutUrl());
            if (getApplicationUrl() != null) {
                logoutUrlBuilder.append("?service=");
                final String encodedKey = URLEncoder.encode(getApplicationUrl(), StandardCharsets.UTF_8.toString());
                logoutUrlBuilder.append(encodedKey);
                LOG.debug("url to reach : {}", logoutUrlBuilder);
            }
            ((HttpServletResponse) servletResponse).sendRedirect(logoutUrlBuilder.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void init() {
        super.init();
        setCasServerLogoutUrl(getString(ConfigurationKeys.CAS_SERVER_LOGIN_URL).replace("/login", "/logout"));
        reloadParameters();
    }

    private void reloadParameters() {
        // Create a seam context if needed
        boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        setApplicationUrl(PreferenceService.getString(APPLICATION_URL));
        if (createContexts) {
            Lifecycle.endCall();
        }
    }
}