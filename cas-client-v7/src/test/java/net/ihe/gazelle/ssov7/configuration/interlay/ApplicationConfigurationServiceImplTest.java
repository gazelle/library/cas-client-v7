package net.ihe.gazelle.ssov7.configuration.interlay;

import net.ihe.gazelle.ssov7.configuration.domain.ApplicationConfigurationService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ApplicationConfigurationServiceImplTest {

    ApplicationConfigurationService applicationConfigurationService;
    private GumConfigClient gumConfigClient;
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        gumConfigClient = Mockito.mock(GumConfigClient.class);
        applicationConfigurationService = new ApplicationConfigurationServiceImpl(gumConfigClient);
    }

    @Test
    public void isUserRegistrationEnabled() {
        when(gumConfigClient.getUserRegistrationEnabled()).thenReturn(true);
        assertTrue(applicationConfigurationService.isUserRegistrationEnabled());
    }

    @Test
    public void isUserRegistrationEnabledClientError() {
        when(gumConfigClient.getUserRegistrationEnabled()).thenThrow(new GumConfigClientException());
        assertFalse(applicationConfigurationService.isUserRegistrationEnabled());
    }

    @Test
    public void getUserRegistrationUrl() {
        String url = "http://my-user-management/registration-url";
        environmentVariables.set("GUM_REGISTRATION_URL", url);
        assertEquals(url, applicationConfigurationService.getUserRegistrationUrl());
    }

    @Test(expected = IllegalStateException.class)
    public void getUserRegistrationUrlMissingVariable() {
        applicationConfigurationService.getUserRegistrationUrl();
    }

    @Test
    public void getUserAccountUrl() {
        String url = "http://my-user-management/account-url";
        environmentVariables.set("GUM_ACCOUNT_URL", url);
        assertEquals(url, applicationConfigurationService.getUserAccountUrl());
    }

    @Test(expected = IllegalStateException.class)
    public void getUserAccountUrlMissingVariable() {
        applicationConfigurationService.getUserAccountUrl();
    }

    @Test
    public void getUsersManagementUrl() {
        String url = "http://my-user-management/users-url";
        environmentVariables.set("GUM_USERS_URL", url);
        assertEquals(url, applicationConfigurationService.getUsersManagementUrl());
    }

    @Test(expected = IllegalStateException.class)
    public void getUsersManagementUrlMissingVariable() {
        applicationConfigurationService.getUsersManagementUrl();
    }

    @Test
    public void shouldShowNewMenu() {
        when(gumConfigClient.getGumMajorVersion()).thenReturn(3);
        assertFalse(applicationConfigurationService.shouldShowNewMenu());

        when(gumConfigClient.getGumMajorVersion()).thenReturn(4);
        assertTrue(applicationConfigurationService.shouldShowNewMenu());

        when(gumConfigClient.getGumMajorVersion()).thenReturn(10);
        assertTrue(applicationConfigurationService.shouldShowNewMenu());

        when(gumConfigClient.getGumMajorVersion()).thenReturn(null);
        assertFalse(applicationConfigurationService.shouldShowNewMenu());
    }
}
