package net.ihe.gazelle.ssov7.registration.interlay.client;

import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.application.ServiceBuilder;
import net.ihe.gazelle.metadata.domain.Service;

public class MetadataServiceProviderMock implements MetadataServiceProvider {

    @Override
    public Service getMetadata() {

        return new ServiceBuilder()
                .setName("service-name")
                .setVersion("1.0.0-SNAPSHOT")
                .setReplicaId("abc12")
                .setInstanceId("azerty1234")
                .build();
    }
}
