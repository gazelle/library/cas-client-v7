package net.ihe.gazelle.ssov7.configuration.interlay;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GumConfigClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private GumConfigClient gumConfigClient;

    @Before
    public void setUp() {
        environmentVariables.set("GUM_REST_API_URL", wireMockRule.baseUrl() + "/gum");
        gumConfigClient = new GumConfigClient();
    }

    @Test
    public void testGumConfigClient() {
        String jsonBody = "{\"userRegistrationEnabled\":true}";
        wireMockRule.stubFor(get("/gum/rest/configurations").willReturn(ok().withBody(jsonBody)));
        assertTrue(gumConfigClient.getUserRegistrationEnabled());
    }

    @Test(expected = GumConfigClientException.class)
    public void testGumConfigClientNotFound() {
        wireMockRule.stubFor(get("/gum/rest/configurations").willReturn(notFound()));
        gumConfigClient.getUserRegistrationEnabled();
    }

    @Test()
    public void testGumConfigClientGetGumMajorVersion() {
        wireMockRule.stubFor(get("/gum/metadata").willReturn(ok()
                .withBody("{\"name\":\"gazelle-user-management\",\"version\":\"4.0.0\"}")));
        assertEquals(new Integer(4), gumConfigClient.getGumMajorVersion());
    }

    @Test(expected = GumConfigClientException.class)
    public void testGumConfigClientGetGumMajorVersionError() {
        wireMockRule.stubFor(get("/gum/metadata").willReturn(notFound()));
        gumConfigClient.getGumMajorVersion();
    }


}